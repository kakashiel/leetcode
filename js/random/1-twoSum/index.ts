
function twoSum(nums: number[], target: number): number[] {
  const lookup = {}
  nums.forEach((e,i) => lookup[e] = i)
  let res=[];
  nums.forEach((e,i) => {
    const cal = target - e
    
    if (lookup[cal] !== undefined && i !== lookup[cal]){
    res= [lookup[cal], i]
      return

    }
  })

  return res;
};
console.log(twoSum([2,7,11,15], 9), [0,1]);
console.log(twoSum([3,2,4], 6), [1,2]);
console.log(twoSum([3,3], 6), [0,1]);

