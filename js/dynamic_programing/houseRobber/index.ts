function rob(nums: number[]): number {
  if (!nums[1]) return nums[0]
  const dp = [];
  dp.push(nums[0])
  dp.push(Math.max(nums[0], nums[1]))
  for (let i = 2; i < nums.length; i++) {
    dp.push(Math.max(dp[i - 1], dp[i - 2] + nums[i]))
  }
  return dp[nums.length - 1]
}

console.log(rob([1, 2, 3, 1]) + " = 4");
console.log(rob([2, 7, 9, 3, 1]) + " = 12");
console.log(rob([2, 1, 1, 2]) + " = 4");
