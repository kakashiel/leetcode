function rob1(nums: number[]): number {
  if (!nums[1]) return nums[0]
  const dp = [];
  dp.push(nums[0])
  dp.push(Math.max(nums[0], nums[1]))
  for (let i = 2; i < nums.length; i++) {
      dp.push(Math.max(dp[i - 1], dp[i - 2] + nums[i]))
  }
  return dp[nums.length - 1]
};

function rob(nums: number[]): number {
  if (!nums[1]) return nums[0]
  const arr1 = [...nums]
  arr1.splice(0, 1)
  nums.splice(nums.length - 1, 1)
  return Math.max(rob1(arr1), rob1(nums))
};

console.log(rob([2,3,2]) + '= 3');
console.log(rob([1,2,3,1]) + '= 4');
console.log(rob([1,2,3]) + '= 3');

