
function climbStairs(n: number): number {
  const dp = []
  dp.push(1)
  dp.push(2)
  for (let i = 2; i < n; i++) {
    dp.push(dp[i - 1] + dp[i - 2])
    
  }
  return dp[n - 1]
}
console.log(climbStairs(2) + '= 2');
console.log(climbStairs(3) + '= 3');
console.log(climbStairs(4) + '= 5');
console.log(climbStairs(1) + '= 1');

