function wordBreak(s: string, wordDict: string[]): boolean {
  const dp = Array(s.length + 1).fill(false);

  for (let index = 0; index < dp.length; index++) {
    if (dp[index] || index === 0) {
      for (let j = 0; j < wordDict.length; j++) {
        const subString = s.slice(index, s.length);
        // console.log(index + "= index");
        
        // console.log(subString);
        //TODO: change s.startWith should be s[index]

        if (subString.startsWith(wordDict[j]) && index + wordDict[j].length <= s.length)
          dp[index + wordDict[j].length] = true;
      }
        // console.log(dp);
    }
  }

  // console.log(dp);
  return dp[dp.length - 1];
}

console.log(wordBreak("leetcode", ["leet", "code"]) + "= true");
console.log(wordBreak('applepenapple',["apple","pen"]) + '= true')
console.log(wordBreak('catsandog', ["cats","dog","sand","and","cat"]) + '= false')
