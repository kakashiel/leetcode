function coinChange(coins: number[], amount: number): number {
  const dp = [0];
  let index = 1;
  while (index <= amount) {
    let minCoin = -2;
    for (let indexCoin = 0; indexCoin < coins.length; indexCoin++) {
      const previous = index - coins[indexCoin];
      // console.log('previous =' + previous);
      // console.log('dp[previous =' + dp[previous]);
      if (previous < 0) continue;
      if (dp[previous] === -1) continue;
      if (minCoin === -2) minCoin = dp[previous] + 1;
      else minCoin = Math.min(dp[previous] + 1, minCoin);
    }
    if (minCoin === -2) dp.push(-1);
    else dp.push(minCoin);
    index++;
  }
  // console.log(dp);
  return dp[dp.length - 1];
}
console.log(coinChange([1, 2, 5], 11) + "= 3");
console.log(coinChange([2], 3) + "= -1");
console.log(coinChange([1], 0) + "= 0");
console.log(coinChange([2], 1) + "= -1");
console.log(coinChange([389, 351, 26, 449, 408, 101], 5528) + "= 14");
