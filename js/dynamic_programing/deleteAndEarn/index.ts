function deleteAndEarn(nums: number[]): number {
  nums.sort((cu, prev) => cu - prev);

  const duplicates: Record<number, number> = {};
  nums.forEach((num) => {
    let counter = duplicates[num] || 0;
    duplicates[num] = ++counter;
  });

  const cost = [0, 0];
  let index = 2;
  // console.log(duplicates);

  for (const [key, value] of Object.entries(duplicates)) {
    const previousNumber = Number(key) - 1;
    const currentCost = Number(key) * value;
    const prevCost = cost[index - 1];
    const prevprevCost = cost[index - 2];
    if (!duplicates[previousNumber]) cost.push(prevCost + currentCost);
    else {
      const totalCurrent = currentCost + prevprevCost;
      // console.log('prevprev=' + prevprevCost);
      // console.log('prev=' + prevCost);
      // console.log('currentCost=' + currentCost);
      // console.log('totalCurrent=' + totalCurrent);
      if (prevCost < totalCurrent) cost.push(totalCurrent);
      else cost.push(prevCost);
    }
    index++;
  }
  // console.log(cost);

  return cost[cost.length - 1];
}
// console.log(deleteAndEarn([3, 4, 2]) + " = 6");
// console.log(deleteAndEarn([2, 2, 3, 3, 3, 4]) + " = 9");
console.log(deleteAndEarn([1,1,1,2,4,5,5,5,6]) + " = 18");
