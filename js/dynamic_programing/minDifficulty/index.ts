function minDifficulty(jobDifficulty: number[], d: number): number {
  if (d > jobDifficulty.length) return -1;
  const dp = Array(d)
    .fill(null)
    .map(() => Array(jobDifficulty.length).fill(1001));
  const n = dp.length;
  const m = dp[0].length;

  for (let j = m - 1; j >= 0; j--) {
    if (dp[n - 1][j + 1] === undefined) dp[n - 1][j] = jobDifficulty[j];
    else dp[n - 1][j] = Math.max(jobDifficulty[j], dp[n - 1][j + 1]);
  }

  for (let i = n - 2; i >= 0; i--) {
    const jstart = m - (n - 1 - i) - 1;
    for (let j = jstart; j >= 0; j--) {
      let min = 1001;
      // console.log(jstart);
      for (let h = j + 1; h < jstart + 2; h++) {
        min = Math.min(min, dp[i + 1][h]);
      }
      console.log(`current: ${min + jobDifficulty[j]} ==${dp[i][j + 1]} before`);

      // if (dp[i][j + 1] !== -1) dp[i][j] = Math.min(min + jobDifficulty[j], dp[i][j + 1]);
      // else dp[i][j] = min + jobDifficulty[j];
    }
  }
  console.log(dp);
  return dp[0][0];
}

// console.log(minDifficulty([6,5,4,3,2,1],2)+ '= 7');
// console.log(minDifficulty([9,9,9], 4)+ '= -1');
// console.log(minDifficulty([1,1,1], 3)+ '= 3');
// console.log(minDifficulty([6, 5, 10, 3, 2, 1], 3) + "= 13");
// console.log(minDifficulty([11,111,22,222,33,333,44,444], 6) + "= 843");
// console.log(minDifficulty([1, 4, 2, 6, 3, 8], 6) + "= 843");
