function maximalSquare(matrix: string[][]): number {
  const m = matrix.length;
  const n = matrix[0].length;
  const dp = Array(m)
    .fill(null)
    .map(() => Array(n).fill(-1));
  // console.log(matrix);
  for (let i = 0; i < m; i++) {
    for (let j = 0; j < n; j++) {
      if (i === 0 || j === 0) dp[i][j] = Number(matrix[i][j]);
      else if (matrix[i][j] === "0") dp[i][j] = 0;
      else {
        const up = dp[i - 1][j];
        const left = dp[i][j - 1];
        const diag = dp[i - 1][j - 1];
        if (up > 0 && up === left && left === diag) dp[i][j] = up + 1;
        else dp[i][j] = Math.min(up, left, diag) + 1;
      }
    }
  }
  let max = 0;
  for (let i = 0; i < dp.length; i++) {
    for (let j = 0; j < dp[0].length; j++) {
      max = max > dp[i][j] ? max : dp[i][j]
    }
    
  }
  // console.log(dp);

  return Math.pow(max, 2);
}

console.log(
  "4=" +
    maximalSquare([
      ["1", "0", "1", "0", "0"],
      ["1", "0", "1", "1", "1"],
      ["1", "1", "1", "1", "1"],
      ["1", "0", "0", "1", "0"],
    ])
);
console.log(
  "1=" +
    maximalSquare([
      ["0", "1"],
      ["1", "0"],
    ])
);
console.log("0=" + maximalSquare([["0"]]));
console.log(
  "16=" +
    maximalSquare([
      ["1", "1", "1", "1", "0"],
      ["1", "1", "1", "1", "0"],
      ["1", "1", "1", "1", "1"],
      ["1", "1", "1", "1", "1"],
      ["0", "0", "1", "1", "1"],
    ])
);
