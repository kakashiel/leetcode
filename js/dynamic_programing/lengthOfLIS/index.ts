function lengthOfLIS(nums: number[]): number {
  const dp = [];
  for (let i = 0; i < nums.length; i++) {
    let max = 0;
    for (let j = 0; j < dp.length; j++) {
      if (nums[j] < nums[i]) max = Math.max(max, dp[j]);
    }
    dp.push(max + 1);
  }

  // console.log(dp);
  return dp.reduce((prev, current) => Math.max(prev, current));
}

console.log(lengthOfLIS([10, 9, 2, 5, 3, 7, 101, 18]) + "= 4");
console.log(lengthOfLIS([0, 1, 0, 3, 2, 3]) + "= 4");
console.log(lengthOfLIS([7, 7, 7, 7, 7, 7, 7]) + "= 1");
