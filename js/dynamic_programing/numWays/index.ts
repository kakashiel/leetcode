
function numWays(n: number, k: number): number {
  const dp = [0, k, k*k];
  for (let index = 3; index < n + 1; index++) {
    dp.push((dp[index - 1] + dp[index - 2])*(k - 1))
  }
  // console.log(dp);
  return dp[dp.length - 1]

};

console.log(numWays(3, 2) + '= 6' );
console.log(numWays(1, 1) + '= 1' );
console.log(numWays(7, 2) + '= 42' );

