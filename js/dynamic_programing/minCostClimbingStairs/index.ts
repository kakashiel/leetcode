function minCostClimbingStairs(cost: number[]): number {
  cost.push(0)
  const dp = [];
  dp.push(0)
  dp.push(Math.min(cost[0], cost[1]))
  for (let i = 2; i < cost.length; i++) {
    dp.push(Math.min(cost[i] + dp[i-1], cost[i - 1] + dp[i-2]))
  }
  
  return dp[dp.length - 1]
};

console.log(minCostClimbingStairs([10,15])+ '= 10');
console.log(minCostClimbingStairs([10,15,20])+ '= 15');
console.log('[10,15,20]');
console.log(minCostClimbingStairs([1,100,1,1,1,100,1,1,100,1])+ '= 6');

