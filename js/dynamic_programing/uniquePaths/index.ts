function uniquePaths(m: number, n: number): number {
  const dp = Array(n)
    .fill(null)
    .map(() => Array(m).fill(0));
  dp.forEach((arr) => (arr[0] = 1));
  dp[0].forEach((ele, index) => (dp[0][index] = 1));
  for (let i = 1; i < m; i++) {
    for (let j = 1; j < n; j++) {
      dp[j][i] = dp[j - 1][i]  + dp[j][i - 1]
    }
  }
  return dp[n - 1][m - 1];
}

console.log(uniquePaths(3, 2) + "= 3");
console.log(uniquePaths(3, 7) + "= 28");
