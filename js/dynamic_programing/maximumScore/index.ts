function maximumScoreRec(nums: number[], multipliers: number[]): number {
  const max = {};
  function dp(i: number, left: number) {
    if (i === multipliers.length) return 0;
    const multi = multipliers[i];
    const right = nums.length - 1 - (i - left);

    if (!max[`${i}, ${left}`]) {
      console.log("nomemo");

      const currLeft = multi * nums[left] + dp(i + 1, left + 1);
      const currRight = multi * nums[right] + dp(i + 1, left);
      max[`${i}, ${left}`] = Math.max(currLeft, currRight);
    } else console.log("memo");
    return max[`${i}, ${left}`];
  }
  return dp(0, 0);
}

//TODO ahah
function maximumScore(nums: number[], multipliers: number[]): number {
  const max = [];

  for (let i = 0; i < multipliers.length; i++) {
    for (let j = 0; j < i; j++) {
      const multi = multipliers[i];
      const right = nums.length - 1 - (i - left);
    }
  }

  if (i === multipliers.length) return 0;

  if (!max[`${i}, ${left}`]) {
    console.log("nomemo");

    const currLeft = multi * nums[left] + dp(i + 1, left + 1);
    const currRight = multi * nums[right] + dp(i + 1, left);
    max[`${i}, ${left}`] = Math.max(currLeft, currRight);
  } else console.log("memo");
  return max[`${i}, ${left}`];
}

console.log(maximumScore([1, 2, 3], [3, 2, 1]) + "= 14");
console.log(maximumScore([-5, -3, -3, -2, 7, 1], [-10, -5, 3, 4, 6]) + "= 103");
