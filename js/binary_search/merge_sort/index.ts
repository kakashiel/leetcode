const startMergeSort = (array: number[]) => {
  const merge = (array: number[], left: number, mid: number, right: number) => {
    const leftArr = [];
    for (let index = left; index <= mid; index++) {
      leftArr.push(array[index])
      
    }
    const rightArr = [];
    for (let index = mid + 1; index <= right; index++) {
      rightArr.push(array[index])
      
    }
    let leftIndex = 0;
    let rightIndex = 0;
    let index = left;
    
    while (leftIndex < leftArr.length && rightIndex < rightArr.length) {
      
      if (leftArr[leftIndex] < rightArr[rightIndex]) {
        array[index] = leftArr[leftIndex];
        leftIndex++;
      } else {
        array[index] = rightArr[rightIndex];
        rightIndex++;
      }
      index++;
    }
    while (leftIndex < leftArr.length) {
      array[index] = leftArr[leftIndex];
      leftIndex++;
      index++;
    }
    while (rightIndex < rightArr.length) {
      array[index] = rightArr[rightIndex];
      rightIndex++;
      index++;
    }
  };
  const mergeSort = (array: number[], left: number, right: number): number[] => {
    if (right <= left) return;
    // const mid = Math.floor((right + left) / 2);
    const mid = Math.floor(left + (right - left )/2);
    
    mergeSort(array, left, mid);
    mergeSort(array, mid + 1, right);
    merge(array, left, mid, right);
    return array;
  };
  return mergeSort(array, 0, array.length - 1)
};

console.log(startMergeSort([4, 1, 2, 3]) + "= 1,2,3,4");
console.log(startMergeSort([]) + "= ");
console.log(startMergeSort([1]) + "= 1");
console.log(startMergeSort([1,2,3]) + "= 1,2,3");
console.log(startMergeSort([0,111, 22,12, 313,3 ,12,4,12,39]) + "= ");


// console.log([1, 2].slice(0, 1));

