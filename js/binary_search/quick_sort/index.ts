

const startQuickSort=(array: number[]) => {

  const swap = (array, i, j)  => {
        const tmp = array[i]
        array[i] = array[j]
        array[j] = tmp;
  }

  const partionner = (array: number[], left:number, right: number): number => {
    // console.log('left' + left);
    // console.log('right' + right);
    
    const pivot = array[right];
    let i = left - 1;
    for (let j = left; j < right; j++) {
      if (pivot > array[j]) {
        i++;
        swap(array, i, j)
      }
    }
    swap(array, i + 1, right)
    return i + 1;

  }
  const quickSort = (array: number[], left: number, right: number) => {
    if (left >= right) return;
    const pi = partionner(array, left, right)
    quickSort(array, left, pi - 1)
    quickSort(array, pi + 1, right)

  }

  quickSort(array, 0, array.length - 1)
  return array;

}

console.log(startQuickSort([4, 1, 2, 3]) + "= 1,2,3,4");
console.log(startQuickSort([]) + "= ");
console.log(startQuickSort([1]) + "= 1");
console.log(startQuickSort([1,2,3]) + "= 1,2,3");
console.log(startQuickSort([0,111, 22,12, 313,3 ,12,4,12,39]) + "= ");
