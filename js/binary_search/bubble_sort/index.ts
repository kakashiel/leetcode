
const bubble_sort = (array: number[]) => {
  for (let index = 0; index < array.length - 1; index++) {
    for (let i = 0; i < array.length - 1; i++) {
      if (array[i] > array[i + 1]) {
        const tmp = array[i+1]
        array[i +1] = array[i]
        array[i] = tmp
      }
    }
  }
  return array
}
console.log(bubble_sort([3,2,4,1])+ '= 1,2,3,4');
console.log(bubble_sort([1])+ '= 1');
console.log(bubble_sort([])+ '= ');
console.log(bubble_sort([1,2,3,4])+ '= 1,2,3,4');
