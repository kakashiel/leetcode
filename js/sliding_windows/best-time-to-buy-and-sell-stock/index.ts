function maxProfit(prices: number[]): number {
  let res = 0;
  let previous = Number.MAX_VALUE;

  for (let index = 0; index < prices.length; index++) {
    const element = prices[index];
    res = element - previous > res ? element - previous : res;
    if (element < previous) {
      previous = element;
    }
  }
  return res;
}

console.log(maxProfit([1, 8, 2, 5]) + "= 7");
console.log(maxProfit([7, 1, 5, 3, 6, 4]) + "= 5");
console.log(maxProfit([7, 6, 4, 3, 1]) + "= 0");
