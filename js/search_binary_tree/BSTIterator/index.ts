
import {TreeNode, tests, TreeNodeBuilder} from '../../utils/tree'

class BSTIterator {
  constructor(root: TreeNode | null) {

  }

  next(): number {

  }

  hasNext(): boolean {

  }
}


const bSTIterator = new BSTIterator(TreeNodeBuilder([7, 3, 15, null, null, 9, 20]))


bSTIterator.next();    // return 3
bSTIterator.next();    // return 7
bSTIterator.hasNext(); // return True
bSTIterator.next();    // return 9
bSTIterator.hasNext(); // return True
bSTIterator.next();    // return 15
bSTIterator.hasNext(); // return True
bSTIterator.next();    // return 20
bSTIterator.hasNext(); // return False


