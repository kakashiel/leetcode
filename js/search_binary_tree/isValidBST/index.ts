
import { TreeNode, tests} from './../../utils/tree'


function path(current: TreeNode | null, min: number, max: number) {
  if (current === null) return true
  // console.log(current.val);
  // console.log(min);
  // console.log(max);
  // console.log(current.val < min);
  // console.log(max < current.val);
  // console.log(
  //   '===================='
  // );
  if (current.val < min || max < current.val) return false;
  return path(current.left, min, current.val) && path(current.right, current.val, max)

}
function isValidBST(root: TreeNode | null): boolean {
  if (root === null) return true
  return path(root, - Number.MAX_VALUE, Number.MAX_VALUE)

};
// tests.searchTree1(isValidBST, 'true')
// tests.searchTree2(isValidBST, 'false')
tests.searchTree3(isValidBST, 'true')
tests.test3(isValidBST, 'true')
// tests.test4(isValidBST, 'false')
