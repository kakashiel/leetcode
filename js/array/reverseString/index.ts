
function reverseString(s: string[]): void {
  let i = 0;
  let j = s.length - 1;
  while( i < j){
    const tmp = s[j]
    s[j]  = s[i]
    s[i] = tmp;
    i++;
    j--;
  }
};

const test = () => {
  const ar = ["h","e","l","l","o"]
  console.log(reverseString(ar));
  console.log(ar);
  
  console.log(["o","l","l","e","h"]);
  const han = ["H","a","n","n","a","h"]
  console.log(reverseString(han));
  console.log(han);
  console.log(["h","a","n","n","a","H"]);
}

test()
