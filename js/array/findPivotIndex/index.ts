const absolute = (x: number) => (x >= 0 ? x : -x);
function pivotIndex(nums: number[]): number {
  const rightPath = [];
  const leftPath = [];
  let previousLeft = 0;
  let previousRight = 0;
  for (let i = 0; i < nums.length; i++) {
    leftPath.push(previousLeft);
    rightPath.push(previousRight);
    previousLeft += nums[i];
    previousRight += nums[nums.length - 1 - i];
  }
  console.log(leftPath);
  console.log(rightPath);
  
  
  for (let i = 0; i < leftPath.length; i++) {
    
    if (leftPath[i] === rightPath[rightPath.length - 1 - i]) return i;
  }
  return -1;
}

// console.log(`${absolute(-1)} = 1`);
// console.log(`${absolute(1)} = 1`);
// console.log(`${pivotIndex([1, 7, 3, 6, 5, 6])} = 3`);
// console.log(`${pivotIndex([1, 2, 3])} = -1`);
// console.log(`${pivotIndex([2, 1, -1])} = 0`);
// console.log(`${pivotIndex([-1, -1, -1, -1, -1, 0])} = 2`);
// console.log(`${pivotIndex([-1, -1, -1, 0, 1, 1])} = 0`);
console.log(`${pivotIndex([-1,-1,0,1,0,-1])} = 4`);
console.log(`${pivotIndex([-1,-1,0,1,1,0])} = 5`);
