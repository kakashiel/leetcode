
function addBinary(a: string, b: string): string {
  let res = '';
  let index = 0;
  let carry = 0;
  while (index < a.length || index < b.length){
    const aElem = Number(a[a.length - 1 - index]) || 0
    const bElem = Number(b[b.length - 1 - index]) || 0
    let additin = aElem + bElem + carry;
    
    carry = additin >= 2 ? 1 : 0;
    let unit = additin === 1 || additin === 3 ? 1 : 0;
    res = unit + res
    index++;
  }
  if (carry)
  res = '1' + res;
  return res;
};

const test = () => {
  console.log(addBinary("1010", "1011") + ' = 10101');
}
test()

const test1 = () => {
  console.log(addBinary("11", "1") + ' = 100');
}
test1()
