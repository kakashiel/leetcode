
function minSubArrayLen(target: number, nums: number[]): number {
  let total = 0;
    let left = 0;
  let res = nums.length + 1
  for (let right = 0; right < nums.length; right++) {
    total += nums[right]
    while (total >= target){
      const currentLaps = right - left + 1 
      res = currentLaps < res ? currentLaps : res;
      total -= nums[left]
      left++
    }
  }
  return res === nums.length + 1 ? 0 : res; 
};

console.log(minSubArrayLen(7, [2,3,1,2,4,3]));
console.log(2);

console.log(minSubArrayLen(4, [1,4,4]));
console.log(1);

console.log(minSubArrayLen(11, [1,1,1,1,1,1,1,1]));
console.log(0);

console.log(minSubArrayLen(11, [1,2,3,4,5]));
console.log(3);

