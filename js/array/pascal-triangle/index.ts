function generate(numRows: number): number[][] {
  const res = [];
  if (numRows >= 1) res.push([1]);
  if (numRows >= 2) res.push([1, 1]);
  for (let index = 2; index < numRows; index++) {
    const tmp = [1];
    const previous = res[index - 1];
    for (let j = 0; j < previous.length - 1; j++) {
      const element = previous[j] + previous[j + 1];
      tmp.push(element)
    }
    tmp.push(1)
    res.push(tmp)
  }
  return res;
}

const test = () => {
  const res = generate(5);
  console.log("[[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]");
  console.log(res);
};
test();

const test1 = () => {
  const res = generate(1);
  console.log("[[1]]");
  console.log(res);
};
test1();
