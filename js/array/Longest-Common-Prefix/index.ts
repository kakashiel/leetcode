
function longestCommonPrefix(strs: string[]): string {
  let prefix = strs[0];
  for (let i = 1; i < strs.length; i++) {
    let index = 0;
    while ( index <= prefix.length && strs[i].substring(0, index) === prefix.substring(0, index)){
      index++
    }
    prefix = prefix.substring(0, index - 1)
  }
  return prefix
};

const test = () => {
  console.log(longestCommonPrefix(["flower","flow","flight"]) + '= fl');
  console.log(longestCommonPrefix(["dog","racecar","car"]) + '= ');
  console.log(longestCommonPrefix(["",""]) + '= ');
  console.log(longestCommonPrefix(["flower","flower","flower","flower"]) + '= flower');
}
test()
