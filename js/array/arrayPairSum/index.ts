
function arrayPairSum(nums: number[]): number {
  const sort = nums.sort((a, b) => (a -b))
  let res = 0;
  for (let i = 0; i < sort.length; i+=2) {
    res += sort[i] > sort[i + 1] ? sort[i + 1] : sort[i]
  }
  return res;
};

const test = () => {
  const nums = [1,4,3,2]
  console.log(arrayPairSum(nums) + '= 4');
  
  const nums2 = [6,2,6,5,1,2]
  console.log(arrayPairSum(nums2) + '= 9');
}
test()
