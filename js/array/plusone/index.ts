function plusOne(digits: number[]): number[] {
  let incrementer = 1;
  const newArray = [];
  for (let i = digits.length - 1; i >= 0; i--) {
    const element = digits[i] + incrementer;
    if (element > 9) incrementer = 1;
    else incrementer = 0;
    
    newArray.push(element % 10);
  }
  if (incrementer) newArray.push(incrementer);
  
  const res = [];
  for (let i = newArray.length - 1; i >= 0; i--) {
    res.push(newArray[i])
  }
  return res
}

const test = () => {
  const array = [9];
  console.log(plusOne(array));
};
test()

const test2 = () => {
  const array = [0];
  console.log(plusOne(array));
};
test2()

const test3 = () => {
  const array = [];
  console.log(plusOne(array));
};
test3()
