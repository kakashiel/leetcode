function isPalindrome(s: string): boolean {
  let ss = s.replaceAll(/[^a-zA-Z0-9]/g, "");
  ss = ss.toLowerCase()
  for (let index = 0; index < ss.length / 2; index++) {
    if (ss[index] !== ss[ss.length - index - 1])
    return false

  }

  return true;
}
console.log(isPalindrome("A man, a plan, a canal: Panama") + "= true");
console.log(isPalindrome("race a car") + "= false");
console.log(isPalindrome(" ") + "= true");
