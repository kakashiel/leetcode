function strStr(haystack: string, needle: string): number {
  if (needle === "") return 0;
  return haystack.indexOf(needle);
}

const test = () => {
  console.log(strStr("hello", "ll") + "= 2");
  console.log(strStr("hello", "") + "= 0");
  console.log(strStr("hello", "li") + "= -1");
};
test();
