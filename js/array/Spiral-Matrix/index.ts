type GoReturn = {
  res: number[];
  x: number;
  y: number;
};
function goRight(matrix: number[][], x: number, y: number, max: number): GoReturn {
  const res = [];
  for (let index = x; index < max; index++) {
    res.push(matrix[y][x]);
    console.log(`row: ${x}, col: ${y}`);
    x++;
  }

  return { res, x: --x, y: ++y };
}

function goDown(matrix: number[][], x: number, y: number, max: number): GoReturn {
  const res = [];
  for (let index = y; index < max; index++) {
    res.push(matrix[y][x]);
    console.log(`row: ${x}, col: ${y}`);
    y++;
  }
  return { res, x: --x, y: --y };
}

function goLeft(matrix: number[][], x: number, y: number, max: number): GoReturn {
  const res = [];
  for (let index = x; index > max; index--) {
    res.push(matrix[y][x]);
    console.log(`row: ${x}, col: ${y}, val: ${matrix[y][x]}`);
    x--;
  }
  return { res, x: ++x, y: --y };
}

function goUp(matrix: number[][], x: number, y: number, max: number): GoReturn {
  const res = [];
  console.log(`row: ${x}, col: ${y}, max: ${max}`);
  for (let index = y; index > max; index--) {
    res.push(matrix[y][x]);
    console.log(`row: ${x}, col: ${y}, val: ${matrix[y][x]}`);
    y--;
  }
  return { res, x: ++x, y: ++y };
}

function spiralOrder(matrix: number[][]): number[] {
  let result = [];
  const numOfElement = matrix[0].length;
  const numOfArray = matrix.length;

  let boundaryRight = numOfElement;
  let boundaryDown = numOfArray;
  let boundaryLeft = -1;
  let boundaryUp = -1;

  let index = 0;
  let row = 0;
  let col = 0;
  while (true) {
    let resPath = goRight(matrix, row, col, boundaryRight);
    boundaryUp++;
    result = [...result, ...resPath.res];
    row = resPath.x;
    col = resPath.y;
    index += resPath.res.length;
    console.log(result);
    if (index >= numOfElement * numOfArray) break;

    resPath = goDown(matrix, row, col, boundaryDown);
    boundaryRight--;
    result = [...result, ...resPath.res];
    row = resPath.x;
    col = resPath.y;
    index += resPath.res.length;
    console.log(result);
    if (index >= numOfElement * numOfArray) break;

    resPath = goLeft(matrix, row, col, boundaryLeft);
    boundaryDown--;
    result = [...result, ...resPath.res];
    row = resPath.x;
    col = resPath.y;
    index += resPath.res.length;
    console.log(result);
    if (index >= numOfElement * numOfArray) break;

    resPath = goUp(matrix, row, col, boundaryUp);
    boundaryLeft++;
    result = [...result, ...resPath.res];
    row = resPath.x;
    col = resPath.y;
    index += resPath.res.length;
    console.log(index);
    if (index >= numOfElement * numOfArray) break;
  }
  return result;
}

const test = () => {
  const matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
  ];
  console.log("[1,2,3,6,9,8,7,4,5] = " + spiralOrder(matrix));
  console.log('=================================================');
  
};
test();

const test1 = () => {
  const matrix = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12],
  ];
  console.log("[1,2,3,4,8,12,11,10,9,5,6,7] = " + spiralOrder(matrix));
};
test1();
