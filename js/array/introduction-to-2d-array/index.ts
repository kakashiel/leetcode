function findDiagonalOrder(mat: number[][]): number[] {
  let res = [];
  const numberOfArray = mat.length;
  const numberOfElement = mat[0].length;
  for (let i = 0; i < numberOfArray + numberOfElement - 1; i++) {
    let row = i < numberOfElement ? 0 : i - numberOfElement + 1;
    let col = i < numberOfElement ? i : numberOfElement - 1;
    let tmp = [];
    while (col >= 0 && row <= numberOfArray - 1) {
      tmp.push(mat[row][col]);
      row++;
      col--;
    }
    if (i % 2 === 0) {
      
      let rev = [];
      for (let index = tmp.length - 1; index >= 0; index--) {
        rev.push(tmp[index]);
      }
      tmp = rev;
    }
    res = [...res, ...tmp]
  }

  return res;
}

const test = () => {
  const array = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
  ];
  console.log("[1,2,4,7,5,3,6,8,9] = " + findDiagonalOrder(array));
};
test();

const test2 = () => {
  const array = [
    [1, 2],
    [3, 4],
  ];
  console.log("[1,2,3,4] = " + findDiagonalOrder(array));
};
test2();

const test3 = () => {
  const array = [[2,3]]
  console.log("[2,3] = " + findDiagonalOrder(array));
}
test3()
