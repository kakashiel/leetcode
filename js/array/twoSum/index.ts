function twoSum(numbers: number[], target: number): number[] {
  for (let i = 0; i < numbers.length; i++) {
    let index = i + 1;
    let add = -1000;
    while (index < numbers.length) {
      add = numbers[index] + numbers[i];
      if ((add) => target) break;
      index++;
    }
    if (add === target) return [i + 1, index + 1];
  }
}

const test = () => {
  const res = twoSum([2, 7, 11, 15], 9);
  console.log([1, 2]);
  console.log(res);

  const res2 = twoSum([2, 3, 4], 6);
  console.log([1, 3]);
  console.log(res2);

  const res3 = twoSum([-1, 0], -1);
  console.log([1, 2]);
  console.log(res3);
};
test();
