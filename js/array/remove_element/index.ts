function removeElement(nums: number[], val: number): number {
  let k = 0;
  for (let index = 0; index < nums.length; index++) {
    if (nums[index] !== val) {
      nums[k] = nums[index];
      k++;
    }
  }
  return k;
}

const test = () => {
  console.log(removeElement([3, 2, 2, 3], 3));
  console.log([2, 2, 0, 0]);
  console.log(removeElement([0, 1, 2, 2, 3, 0, 4, 2], 2));
  console.log([0, 1, 4, 0, 3, 0, 0, 0]);
};
test()
