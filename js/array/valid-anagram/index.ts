function isAnagram(s: string, t: string): boolean {
  if (s.length != t.length) return false;
  const res = {};
  for (let index = 0; index < s.length; index++) {
    const element = s[index];
    const element2 = t[index];
    res[element] = res[element] !== undefined ? res[element] + 1 : 1;
    res[element2] = res[element2] !== undefined ? res[element2] - 1 : -1;
  }
  return Object.values(res).every(key => (key === 0));
}

// console.log(isAnagram("a", "a") + "= true");
console.log(isAnagram("anagram", "nagaram") + "= true");
