function countComponents(n: number, edges: number[][]): number {
  let nbComponent = 0;
  let adjacent:{[a: number]: number[]} = {};
  for (let index = 0; index < n; index++) {
    adjacent[index] = [];
  }
  for (let index = 0; index < edges.length; index++) {
    const connection = edges[index];
    const a = connection[0];
    const b = connection[1];
    adjacent[a].push(b);
    adjacent[b].push(a);

  }
  let visitied = new Set();
  const dfs = (current: number, parent: number): void  => {
    const neighbour = adjacent[current];
    // console.log("current=" + current);
    // console.log("parent=" + parent);
    
    visitied.add(current);
    for (const neigh of neighbour) {
      if (visitied.has(neigh)){
        if (neigh !== parent){
          continue;
        }

      }else {
          dfs(neigh, current)
      }
    }
  }
  for (const [key, value] of Object.entries(adjacent)) {
    if (!visitied.has(Number(key))) {
      // console.log(visitied);
      
      nbComponent++;
      dfs(Number(key), -1)

    }
  }

  return nbComponent;

};
console.log(countComponents(5, [[0,1],[1,2],[3,4]])+ "= 2");
console.log(countComponents(5, [[0,1],[1,2],[2,3],[3,4]])+ "= 1");
console.log(countComponents(5, [[0,1],[1,2],[0,2],[3,4]])+ "= 2");
console.log(countComponents(5, [[0,1],[0,2],[1,2],[2,3],[2,4]])+ "= 1");


