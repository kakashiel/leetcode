function validTree(n: number, edges: number[][]): boolean {
  const adjList = {};
  for (let i = 0; i < n; i++) {
    adjList[i]= [];
  }
  for (let [a, b] of edges) {
    adjList[a].push(b);
    adjList[b].push(a);
    
  }

  let visited = new Set();
  const checkCycle = (current, parent): boolean => {
    visited.add(current);
    let neighbours = adjList[current];
    if (neighbours.length) {
      for (const neigh of neighbours) {
        if(visited.has(neigh)) {
          if (neigh !== parent) {
            return true;
          }
          else {
            if (checkCycle(neigh, current)) {
              return true;
            }
          }
        }
        
      }
    }
    return false;
  }
  if (checkCycle(0, -1)) return false;
  for (let index = 0; index < n; index++) {
    if (!visited.has(index))
    return false;
    
  }

  return true;
};

console.log(validTree(5, [[0,1],[0,2],[1,3],[1,4]])+"= true");
console.log(validTree(5, [[0,1],[1,2],[2,3],[1,3],[1,4]])+ "= false");

