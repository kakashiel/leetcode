export class TreeNode {
  val: number;
  left: TreeNode | null;
  right: TreeNode | null;
  constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
    this.val = val === undefined ? 0 : val;
    this.left = left === undefined ? null : left;
    this.right = right === undefined ? null : right;
  }
}

function treeLevelOrder(values: number[], i: number) {
  if (i >= values.length) return null;
  const current = new TreeNode(values[i]);

  if (values[i] === null) {
    values.splice(i * 2 + 1, 0, null);
    values.splice(i * 2 + 2, 0, null);
    return null;
  }
  current.left = treeLevelOrder(values, i * 2 + 1);
  current.right = treeLevelOrder(values, i * 2 + 2);

  return current;
}

export function TreeNodeBuilder(values: number[]) {
  return treeLevelOrder(values, 0);
}

// only binay tree
function bfs(root: TreeNode | null): number[] {
  const res: number[] = [];
  if (root === null) return [];
  const queue: TreeNode[] = [];
  queue.push(root);
  while (queue.length != 0) {
    const current = queue.shift();

    current && res.push(current?.val);
    current?.left && queue.push(current.left);
    current?.right && queue.push(current.right);
  }
  return res;
}

// only binay tree
function dfs(root: TreeNode | null): number[] {
  const res: number[] = [];
  if (root === null) return [];
  const stack: TreeNode[] = [];
  stack.push(root);
  while (stack.length !== 0) {
    const current = stack.pop();
    current && res.push(current?.val);
    current?.left && stack.push(current.left);
    current?.right && stack.push(current.right);
  }

  return res;
}

export const testBuilder =
  (array: number[]) =>
  (
    fn: (e: TreeNode | null, ...a: any[]) => any,
    expect?: string,
    ...args: any[]
  ) => {
    console.log("================");
    console.log("Test = " + array);
    const tree = TreeNodeBuilder(array);
    const res = fn(tree, ...args);
    console.log(res + " = " + expect);
    console.log("BFS =>" + bfs(res) + " = " + expect);
    console.log("DFS =>" + dfs(res) + " = " + expect);
  };

const tree1 = [1, null, 2, 3];
const tree2 = [];
const tree3 = [0];
const tree4 = [3, 9, 20, null, null, 15, 7];
const tree5 = [1,2,3,4,5];
const tree6 = [1, 2, 2, null, 3, null, 3];
const tree7 = [1, 2, 3];
const hasPathSum11 = [5, 4, 8, 11, null, 13, 4, 7, 2, null, null, null, 1];
const hasPathSum22 = [1, 2, 3];
const searchTree1 = [2, 1, 3];
const searchTree3 = [2, 2, 2];

export const tests = {
  test1: testBuilder(tree1),
  test2: testBuilder(tree2),
  test3: testBuilder(tree3),
  test4: testBuilder(tree4),
  test5: testBuilder(tree5),
  test6: testBuilder(tree6),
  test7: testBuilder(tree7),
  hasPathSum1: testBuilder(hasPathSum11),
  hasPathSum2: testBuilder(hasPathSum22),
  searchTree1: testBuilder(searchTree1), //true
  searchTree2: testBuilder([5, 1, 4, null, null, 3, 6]), //false
  searchTree3: testBuilder(searchTree3), //false
};
