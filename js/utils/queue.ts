
export class Queue{
  private size = 100;
  private length = 0;
  private array = [...new Array(this.size)].map(e => undefined)
  private head = 0;
  private tail = 0;

  public enqueue(val) {
    if(this.isFull()) return ;
    this.array[this.head] = val
    this.length++
    this.head = (this.head + 1) % this.size;
  }

  public dequeue() {
    if(this.isEmpty()) return undefined
    let ret = this.array[this.tail]
    this.tail = (this.tail + 1) % this.size;
    this.length--
    return ret;
  }

  public isEmpty() {
    return this.length === 0
  }
  public isFull() {
    
    return this.size  === this.length
  }
}
