function topKFrequent(nums: number[], k: number): number[] {
  const hash = {};
  const res = [];
  for (let i = 0; i < nums.length; i++) {
    hash[nums[i]] = hash[nums[i]] === undefined ? 1 : ++hash[nums[i]];
    hash[nums[i]] >= k && res.push(nums[i])
  }
  const res = [...hash];
  
  return [... new Set(res)];
}

console.log(topKFrequent([1, 1, 1, 2, 2, 3], 2) + "= [1,2]");
console.log(topKFrequent([1], 1) + "= [1]");
