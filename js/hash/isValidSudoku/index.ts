function isValidSudoku(board: string[][]): boolean {
  const squares =new Array(9);
  const rows =new Array(9);
  const columns =new Array(9);
  for (var i = 0; i < 9; i++) {
    squares[i] = {}
    rows[i] = {}
    columns[i] = {}
    }
  for (let i = 0; i < board.length; i++) {
    for (let j = 0; j < board[i].length; j++) {
      // console.log(rows);
      // console.log(rows[i]);
      // console.log( j+ '='+ board[i][j]+"="+ rows[i][board[i][j]]);
      
      if (board[i][j] === ".")
      continue;

      const y = 3 * Math.trunc((i/3));
      const x =  Math.trunc((j/3))
      if (rows[i][board[i][j]] || columns[j][board[i][j]] || squares[x+y][board[i][j]]) return false;
      rows[i][board[i][j]] = true;
      columns[j][board[i][j]] = true;
      squares[x+y][board[i][j]] = true;
      
    }
  }

  return true;
}

console.log(
  isValidSudoku([
    ["5", "3", ".", ".", "7", ".", ".", ".", "."],
    ["6", ".", ".", "1", "9", "5", ".", ".", "."],
    [".", "9", "8", ".", ".", ".", ".", "6", "."],
    ["8", ".", ".", ".", "6", ".", ".", ".", "3"],
    ["4", ".", ".", "8", ".", "3", ".", ".", "1"],
    ["7", ".", ".", ".", "2", ".", ".", ".", "6"],
    [".", "6", ".", ".", ".", ".", "2", "8", "."],
    [".", ".", ".", "4", "1", "9", ".", ".", "5"],
    [".", ".", ".", ".", "8", ".", ".", "7", "9"],
  ]) + "= true"
);

console.log(
  isValidSudoku([
    ["8", "3", ".", ".", "7", ".", ".", ".", "."],
    ["6", ".", ".", "1", "9", "5", ".", ".", "."],
    [".", "9", "8", ".", ".", ".", ".", "6", "."],
    ["8", ".", ".", ".", "6", ".", ".", ".", "3"],
    ["4", ".", ".", "8", ".", "3", ".", ".", "1"],
    ["7", ".", ".", ".", "2", ".", ".", ".", "6"],
    [".", "6", ".", ".", ".", ".", "2", "8", "."],
    [".", ".", ".", "4", "1", "9", ".", ".", "5"],
    [".", ".", ".", ".", "8", ".", ".", "7", "9"],
  ]) + "= false"
);
