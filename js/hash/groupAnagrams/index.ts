function groupAnagrams(strs: string[]): string[][] {
  const lookup = {};

  for (let index = 0; index < strs.length; index++) {
  const count = Array(26).fill(0);
    for (let i = 0; i < strs[index].length; i++)
    count[strs[index].charCodeAt(i) - "a".charCodeAt(0)]++;
    
    const key = count.join();

    if (lookup[key] === undefined) lookup[key] = [strs[index]];
    else lookup[key] = [...lookup[key], strs[index]];
  }

  const res = [];
  for (const key in lookup) {
      res.push(lookup[key]);
  }
  return res;
}

console.log(groupAnagrams(["eat", "tea", "tan", "ate", "nat", "bat"]));
console.log([["bat"], ["nat", "tan"], ["ate", "eat", "tea"]]);
console.log(groupAnagrams([""]));
console.log([[""]]);
console.log(groupAnagrams(["a"]));
console.log([["a"]]);
