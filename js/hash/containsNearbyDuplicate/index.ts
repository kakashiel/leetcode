function containsNearbyDuplicate(nums: number[], k: number): boolean {
  const lookup = {};
  for (let index = 0; index < nums.length; index++) {
    const previousSameValueIndex = lookup[nums[index]];
    if (previousSameValueIndex !== undefined) {
      if (index - previousSameValueIndex <= k) return true;
    }
    lookup[nums[index]] = index;
  }
  return false;
}
console.log(containsNearbyDuplicate([1, 2, 3, 1], 3) + "=t");
console.log(containsNearbyDuplicate([1, 0, 1, 1], 1) + "=t");
console.log(containsNearbyDuplicate([1, 2, 3, 1, 2, 3], 2) + "= f");
