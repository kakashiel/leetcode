
function lengthOfLongestSubstring(s: string): number {
  let windowLookup = "";
  const lookup = new Set();
  let max = 0;
  for (let i = 0; i < s.length; i++) {
    if (windowLookup.length > max) max = windowLookup.length;
    //   console.log(windowLookup);
    // console.log(lookup);
    
    while (lookup.has(s[i]) && windowLookup !== ""){
      const firstChar = windowLookup[0]
      windowLookup = windowLookup.slice(1, windowLookup.length)
      lookup.delete(firstChar)
    }
    windowLookup += s[i]
    lookup.add(s[i])
  }
    if (windowLookup.length > max) max = windowLookup.length;
  return max;

};

console.log(lengthOfLongestSubstring("abcabcbb")+ '= 3');
console.log(lengthOfLongestSubstring("bbbbb")+ '= 1');
console.log(lengthOfLongestSubstring("pwwkew")+ '= 3');
console.log(lengthOfLongestSubstring("p")+ '= 3');

