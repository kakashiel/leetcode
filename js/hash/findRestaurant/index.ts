function findRestaurant(list1: string[], list2: string[]): string[] {
  const hash = {};
  for (let index = 0; index < list1.length; index++) {
    hash[list1[index]] = index;
  }
  
  let res = [];
  let minIndex = 99999999;
  for (let index = 0; index < list2.length; index++) {
    if (hash[list2[index]] !== undefined) {
      hash[list2[index]] += index;
      if (hash[list2[index]] < minIndex){
        res = [list2[index]]
        console.log(res);
        minIndex = hash[list2[index]]
      }
      else if (hash[list2[index]] === minIndex){
        res.push(list2[index])
      }
    }
  }
  return res;
}
console.log(
  findRestaurant(
    ["Shogun", "Tapioca Express", "Burger King", "KFC"],
    ["Piatti", "The Grill at Torrey Pines", "Hungry Hunter Steakhouse", "Shogun"]
  )
);
console.log(
  findRestaurant(
    ["Shogun", "Tapioca Express", "Burger King", "KFC"],
    ["KFC", "Shogun", "Burger King"]
  )
);
