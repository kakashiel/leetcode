class MyHashSet {
  private _hash = 1;
  private store = Array(this._hash).fill(null);
  constructor() {}

  private hash(num: number): number {
    return num % this._hash;
  }

  add(key: number): void {
    const hash = this.hash(key);
    let node = this.store[hash];
    const newNode = new LinkedList(key, null);
    if (!node) {
      this.store[hash] = newNode;
      return;
    }

    while (node.next) {
      if (node.value === key) {
        return;
      }
      if (node.next.value === key) {
        return;
      }
      node = node.next;
    }
    node.next = newNode;
  }

  remove(key: number): void {
    const hash = this.hash(key);
    let node = this.store[hash];
    if (!node) return;
    if (node.value === key) {
      this.store[hash] = null;
      return;
    }

    let prev = node;
    node = node.next;
    while (node) {
      if (node.value === key) {
        prev.next = node.next;
        return;
      }
      prev = node;
      node = node.next;
    }
  }

  contains(key: number): boolean {
    const hash = this.hash(key);
    let node = this.store[hash];
    while (node) {
      if (node.value === key) return true;
      node = node.next;
    }
    return false;
  }
}

class LinkedList {
  private _value: number;
  private _next: LinkedList | null;

  constructor(value: number, next: LinkedList | null) {
    this._value = value;
    this._next = next;
  }

  public set next(next: LinkedList | null) {
    this._next = next;
  }

  public get next() {
    return this._next;
  }

  public set value(value: number) {
    this._value = value;
  }

  public get value() {
    return this._value;
  }
}
const set = new MyHashSet();
set.add(1);
set.add(2);
console.log(set.contains(1) + "=true");
console.log(set.contains(3) + "=false");
set.add(2);
console.log(set.contains(2) + "=true");
set.remove(2);
console.log(set.contains(2) + "=true");
// set.remove(2);
// console.log(set.contains(2) + "=true");
