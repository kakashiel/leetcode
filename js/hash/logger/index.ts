class Logger {
  private _logger = new Map();
  constructor() {}

  shouldPrintMessage(timestamp: number, message: string): boolean {
    if (this._logger.has(message) && timestamp - this._logger.get(message) < 10) {
      return false;
    }
    this._logger.set(message, timestamp);
    return true;
  }
}


/**
 * Your Logger object will be instantiated and called as such:
 * var obj = new Logger()
 * var param_1 = obj.shouldPrintMessage(timestamp,message)
 */
