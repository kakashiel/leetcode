function isIsomorphic(s: string, t: string): boolean {
  const lookup = {};
  const lookup2 = {};
  for (let i = 0; i < s.length; i++) {
    if (lookup[s[i]] && lookup[s[i]] !== t[i]) return false;
    if (lookup2[t[i]] && lookup2[t[i]] !== s[i]) return false;
    // console.log(lookup[s[i]]);
    lookup[s[i]] = t[i];
    lookup2[t[i]] = s[i];
  }
  return true;
}
console.log(isIsomorphic("add", "oll"));
console.log(isIsomorphic("add", "olk"));
console.log(isIsomorphic("babc", "baba"));
