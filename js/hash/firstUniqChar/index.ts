function firstUniqChar(s: string): number {
  const lookup = {};
  for (let i = 0; i < s.length; i++) {
    if (lookup[s[i]] === undefined) lookup[s[i]] = 1;
    else lookup[s[i]] = lookup[s[i]]+ 1;
  }
  for (let i = 0; i < s.length; i++) {
    if(lookup[s[i]] === 1) return i
  }
  // console.log(lookup);
  return -1;
}

console.log(firstUniqChar("leetcode") + "= 0");
console.log(firstUniqChar("loveleetcode") + "= 2");
console.log(firstUniqChar("aabb") + "= -1");
console.log(firstUniqChar("dddccdbba") + "= -1");
