function groupStrings(strings: string[]): string[][] {
  const lookup = {};

  
  for (let index = 0; index < strings.length; index++) {
    const str = strings[index]
    let count = Array(str.length).fill(0)
    
    for (let i = 0; i < str.length; i++) {
      count[i] = str.charCodeAt(i) - "a".charCodeAt(0);
    }
    let key = 0
    for (let i = 1; i < str.length; i++) {
      key = count[i] - count[i - 1]
      if (key < 0) key += 26
    }
    
    lookup[key] = !lookup[key] ? ([strings[index]]) : ([...lookup[key], strings[index]]);

  }
  console.log(lookup);
  const res = [];
  for (const key in lookup) {
    res.push(lookup[key]);
  }
  return res;
}
console.log(groupStrings(["abc", "bcd", "acef", "xyz", "az", "ba", "a", "z"]));
console.log([["acef"], ["a", "z"], ["abc", "bcd", "xyz"], ["az", "ba"]]);
// console.log(groupStrings(["a"]));
// console.log([["a"]]);
