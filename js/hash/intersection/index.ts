function intersection(nums1: number[], nums2: number[]): number[] {
  const set1 = new Set<number>(nums1);
  const set2 = new Set<number>(nums2);
  const res = [];
  for (const key of Array.from(set1.keys())) {
    if (set2.has(key)) res.push(key);
  }
  return res;
}

console.log(intersection([1, 2, 2, 1], [2, 2]) + "=2");
console.log(intersection([4, 9, 5], [9, 4, 9, 8, 4]) + "=4,9");
