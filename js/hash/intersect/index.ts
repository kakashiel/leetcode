function intersect(nums1: number[], nums2: number[]): number[] {
  const lookup = {};
  for (let k = 0; k < nums1.length; k++) {
    if (lookup[nums1[k]] === undefined) lookup[nums1[k]] = 1;
    else lookup[nums1[k]] = lookup[nums1[k]] + 1;
  }
  const lookup2 = {}
  for (let k = 0; k < nums2.length; k++) {
    if (lookup2[nums2[k]] === undefined) lookup2[nums2[k]] = 1;
    else lookup2[nums2[k]] = lookup2[nums2[k]] + 1;
  }
  const res = []
  // debugger
  for (const proprety in lookup) {
    if (lookup2[proprety] !== undefined) {
      for (let index = 0; index < Math.min(lookup[proprety], lookup2[proprety]); index++) {
        res.push(proprety)
      }

    }
  }
  return res;
}

console.log(intersect([4,7,9,7,6,7], [5,0,0,6,1,6,2,2,4]) + "= 4,6");
// console.log(intersect([1, 2, 2, 1], [2, 2]) + "= [2,2]");
// console.log(intersect([4, 9, 5], [9, 4, 9, 8, 4]) + "= [4,9]");
