function singleNumber(nums: number[]): number {
  const hash = new Set();
  const hash2 = new Set<number>();
  for (let index = 0; index < nums.length; index++) {
    if (hash2.has(nums[index])) {
      hash2.delete(nums[index]);
    } else {
      hash2.add(nums[index]);
    }
  }
  const values = [...hash2.keys()]
  return values[0]
}

console.log(singleNumber([2,2,1]) + "= 1");
console.log(singleNumber([4,1,2,1,2]) + "= 4");
console.log(singleNumber([1]) + "= 1");
