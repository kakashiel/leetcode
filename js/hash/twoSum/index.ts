function twoSum(nums: number[], target: number): number[] {
  let hash = {};
  for (let i = 0; i < nums.length; i++) {
    if (hash[target - nums[i]] !== undefined) return [hash[target - nums[i]], i];
    hash = { ...hash, [nums[i]]: i };
    console.log(hash);

  }
  return []
}

console.log(twoSum([2, 7, 11, 15], 9));
console.log(twoSum([3, 2, 4], 6));
console.log(twoSum([3, 3], 6));
