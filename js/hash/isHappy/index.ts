
const parseNumber = (n: number): number[] => {
  const res = [];
  let value = n;
  while (value >= 1) {
    res.push(Math.floor(value % 10));
    value /= 10;
  }
  return res;
};

function isHappy(n: number): boolean {
  let set = {};
  var value: number = n;
  while (value !== 1) {
    const array = parseNumber(value);
    value = array.reduce((prev, curr) => prev + Math.pow(curr, 2), 0);
    
    if (set[value]) return false;
    set = {...set, [value]: true}
  }

  return true;
}
console.log(isHappy(100) + "= true");
console.log(isHappy(19) + "= true");
console.log(isHappy(2) + "= false");
console.log(isHappy(7) + "= true");
