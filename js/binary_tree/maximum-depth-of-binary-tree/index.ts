import { TreeNode, tests } from "../../utils/tree";

type stackDepth = {
  node: TreeNode;
  depth: number;
}

function maxDepth(root: TreeNode | null): number {
  let max = 0;
  if (root === null)
    return max;
  const stack: stackDepth[] = [];
  const rootDepth: stackDepth = {node: root, depth: 0};
  stack.push(rootDepth);
  while(stack.length !== 0) {
    const current = stack.pop();
    max = max > current.depth ? max : current.depth;
    current.node && stack.push({node: current.node.right, depth: current.depth + 1})
    current.node && stack.push({node: current.node.left, depth: current.depth + 1})
  }
  return max
}

tests.test4(maxDepth, "3");
