import { TreeNode, TreeNodeBuilder } from "../../utils/tree";

function isSubtreee(root: TreeNode | null, subRoot: TreeNode | null): boolean {
  const stack = [];
  stack.push(root);
  const stackSubTree = [];
  stackSubTree.push(subRoot);
  while (stack.length != 0) {
    const node = stack.pop();
    const subNode = stackSubTree.pop();
    // console.log("node =" + node.val);
    // console.log("subnode =" + subNode.val);

    if (subNode === undefined) return false;
    if (node.val !== subNode.val) return false;
    if (node.left !== null) {
      stack.push(node.left);
      if (subNode.left === null) {
        return false;
      }
      stackSubTree.push(subNode.left);
      if (node.left.val !== subNode.left.val) return false;
    } else if (subNode.left !== null) {
      stackSubTree.push(subNode.left);
    }
    if (node.right !== null) {
      stack.push(node.right);
      if (subNode.right === null) {
        return false;
      }
      stackSubTree.push(subNode.right);
      if (node.right.val !== subNode.right.val) return false;
    } else if (subNode.right !== null) {
      stackSubTree.push(subNode.right);
    }
  }
  return stack.length === stackSubTree.length;
}

function isSubtree(root: TreeNode | null, subRoot: TreeNode | null): boolean {
  const stack = [];
  stack.push(root);
  while (stack.length != 0) {
    const node = stack.pop();
    if (node.val === subRoot.val) {
      if (isSubtreee(node, subRoot)) return true;
    }
    node.left !== null && stack.push(node.left);
    node.right !== null && stack.push(node.right);
  }
  return false;
}

// let tree1 = TreeNodeBuilder([3, 4, 5, 1, 2]);
// let tree2 = TreeNodeBuilder([4, 1, 2]);
// console.log(isSubtree(tree1, tree2) + "= true");
//
// tree1 = TreeNodeBuilder([3, 4, 5, 1, 2, null, null, null, null, 0]);
// tree2 = TreeNodeBuilder([4, 1, 2]);
// console.log(isSubtree(tree1, tree2) + "= false");
//
// tree1 = TreeNodeBuilder([4,5]);
// tree2 = TreeNodeBuilder([4,null,5]);
// console.log(isSubtree(tree1, tree2) + "= false");

let tree1 = TreeNodeBuilder([3, 4, 5, 1, 2]);
let tree2 = TreeNodeBuilder([4, 1, 2, 1]);
console.log(isSubtree(tree1, tree2) + "= false");
