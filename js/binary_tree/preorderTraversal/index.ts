
import {TreeNode, tests} from '../../utils/tree'

function preorderTraversal(root: TreeNode | null): number[] {
  if (root === null) return []
  const res = []
  let current = root
  const stack = []
  while (stack.length || current !== null) {
    // console.log(res);
    // console.log(stack);
    // console.log(current);
    // console.log('--------------');

    while (current !== null){
      res.push(current.val)
      stack.push(current)
      current = current.left
    }
    current = stack.pop()
    current = current.right

  }

  return res;
};

tests.test1(preorderTraversal, '1,2,3')
tests.test2(preorderTraversal, '')
tests.test3(preorderTraversal, '1')
