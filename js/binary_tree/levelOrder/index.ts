import { Queue } from "../../utils/queue";
import { TreeNode, tests } from "../../utils/tree";


interface TreeNodeLevel {
  current: TreeNode | null,
  level: number

}
export class Queue{
  private size = 100;
  private length = 0;
  private array = [...new Array(this.size)].map(e => undefined)
  private head = 0;
  private tail = 0;

  public enqueu(val) {
    if(this.isFull()) return ;
    this.array[this.head] = val
    this.length++
    this.head = (this.head + 1) % this.size;
  }

  public dequeu() {
    if(this.isEmpty()) return undefined
    let ret = this.array[this.tail]
    this.tail = (this.tail + 1) % this.size;
    this.length--
    return ret;
  }

  public isEmpty() {
    return this.length === 0
  }
  public isFull() {
    
    return this.size  === this.length
  }
}

  function levelOrder(root: TreeNode | null): number[][] {

  const res = [];
  if (root === null) return res;
  const queue = new Queue();
  queue.enqueue({current: root, level: 0})

  while(!queue.isEmpty()) {
    
    const current = queue.dequeue()
    if (current.current === null ) continue;
    // console.log(queue);
    

    if (res[current.level] === undefined)
    res.push([])
    res[current.level].push(current.current.val)
    queue.enqueue({current: current.current.left, level: current.level+ 1})
    queue.enqueue({current: current.current.right, level: current.level+ 1})


  }
  
  return res

}

// tests.test1(levelOrder, '')
tests.test2(levelOrder, "");
tests.test3(levelOrder, "1");
tests.test4(levelOrder, "[[3],[9,20],[15,7]]");
