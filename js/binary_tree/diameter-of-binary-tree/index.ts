import { TreeNode, tests } from "../../utils/tree";
function dfsRecurisve(node: TreeNode | null, maxDiameter: any): number {
  if (node === null) return -1;
  const leftDiam = 1 + dfsRecurisve(node.left, maxDiameter);
  const rightDiam = 1 + dfsRecurisve(node.right, maxDiameter)
  maxDiameter.max = leftDiam + rightDiam > maxDiameter.max ? leftDiam + rightDiam : maxDiameter.max;
  // console.log("val=" + node.val);
  // console.log("leftDiam=" + leftDiam);
  // console.log("rightDiam=" + rightDiam);
  // console.log("maxDiameter=" + maxDiameter.diametre);
  
  const res = leftDiam > rightDiam ? leftDiam : rightDiam
  // console.log("res=" + res);
  // console.log('------');
  return res;
  
  

}

function diameterOfBinaryTree(root: TreeNode | null): number {
  if (root === null) return 0;
  let diametre = {max: 0};
  dfsRecurisve(root, diametre);
  return diametre.max
}

tests.test5(diameterOfBinaryTree, "3");
