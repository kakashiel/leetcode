

import {TreeNode, tests} from './../../utils/tree'

const path = (current, targetSum) => {
  if (current === null) {
    return false
  }
  const newTotal = targetSum - current.val
  
  if (current.left === null && current.right === null) {
    const res = (newTotal  === 0)
    return res

  }
  return path(current.left, newTotal) || path(current.right, newTotal)


}

function hasPathSum(root: TreeNode | null, targetSum: number): boolean {
  if (root === null) {
    return false
  }
  return path(root, targetSum)

};

tests.hasPathSum1(hasPathSum, 'true', 22)
tests.hasPathSum2(hasPathSum, 'false', 5)
