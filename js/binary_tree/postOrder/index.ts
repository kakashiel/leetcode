
import { TreeNode, tests } from './../../utils/tree'



function helper(current: TreeNode | null, res: number[]) {

  if (current === null) return;
  helper(current.left, res)
  helper(current.right, res)
  res.push(current.val)

}

function postorderTraversal(root: TreeNode | null): number[] {
  if (root === null) return [] 
  const res = []
  let current = root;
  helper(current, res)


  return res

};

tests.test1(postorderTraversal, "[3,2,1]")
// tests.test2(postorderTraversal, "[]")
// tests.test3(postorderTraversal, "[1]")
