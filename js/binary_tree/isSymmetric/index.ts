
import {TreeNode, tests} from '../../utils/tree'


const right=(current: TreeNode | null, path: number[]) => {
  if(current === null){
    path.push(null)
    return
  } 
  path.push(current.val)
  right(current.right, path)
  right(current.left, path)
}

const left=(current: TreeNode | null, path: number[]) => {
  if(current === null){
    path.push(null)
    return
  } 
  path.push(current.val)
  left(current.left, path)
  left(current.right, path)
}

function isSymmetric(root: TreeNode | null): boolean {
  if(root === null) return true;
  const rightPath = []
  const leftPath = []
  left(root.left, leftPath)
  right(root.right, rightPath)
  if (rightPath.length !== leftPath.length) return false
  return !rightPath.some((e, i) => e !== leftPath[i])

};

tests.test5(isSymmetric, 'true')
tests.test6(isSymmetric, 'false')
