import { TreeNode, TreeNodeBuilder  } from "../../utils/tree";

function isSameTree(p: TreeNode | null, q: TreeNode | null): boolean {
  const queueP = [];
  const queueQ = [];
  if (p === null && q === null) return true;
  if (p === null || q === null) return false;
  queueP.push(p);
  queueQ.push(q);
  while (queueP.length != 0 || queueQ.length !== 0) {
    if(queueP.length !== queueQ.length) return false;
    p = queueP.shift();
    q = queueQ.shift();
    console.log(queueP);
    console.log(queueQ);
    
    if (p.val !== q.val) return false;
    p.left !== null && queueP.push(p.left);
    p.right !== null && queueP.push(p.right);
    q.left !== null && queueQ.push(q.left);
    q.right !== null && queueQ.push(q.right);
  }

  return true;
};

const tree1 = TreeNodeBuilder([1,2,3])
const tree2 = TreeNodeBuilder([1,2])
const tree3 = TreeNodeBuilder([1,null, 2])
const tree4 = TreeNodeBuilder([1])
const tree5 = TreeNodeBuilder([1,null,2])

console.log(isSameTree(tree1, tree1));
console.log(isSameTree(tree2, tree3));
console.log(isSameTree(tree4, tree5));


