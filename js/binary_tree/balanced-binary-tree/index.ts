import { TreeNode, testBuilder } from "../../utils/tree";


function dfs(node: TreeNode | null): number {
  if (node === null) return 0;
  const nodeLeft = 1 + dfs(node.left);
  const nodeRight = 1 + dfs(node.right);
  const isBalanced = Math.abs(nodeLeft - nodeRight) <= 1;
  if (!isBalanced) return -1000;
  

  const maxDepth = Math.max(nodeLeft, nodeRight);
  return maxDepth

}

function isBalanced(root: TreeNode | null): boolean {
  if (root === null)
    return true;
  const res = dfs(root);
  console.log(res);
  
  return res > -10;
}

testBuilder([3,9,20,null,null,15,7])(isBalanced, "true")
testBuilder([1,2,2,3,3,null,null,4,4])(isBalanced, "false")
testBuilder([1,2,2,3,null,null,3,4,null,null,4])(isBalanced, "false")
