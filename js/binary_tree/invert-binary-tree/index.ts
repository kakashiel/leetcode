import { TreeNode, tests } from "../../utils/tree";
//
function invertTree(root: TreeNode | null): TreeNode | null {
  if (root === null) return null;
  const queue: TreeNode[] = [];
  queue.push(root);
  while (queue.length !== 0) {
    const current = queue.shift();
    const tmp = current.left;
    current.left = current.right;
    current.right = tmp;
    current.right && queue.push(current.right);
    current.left && queue.push(current.left);
  }

  return root;
}

tests.test7(invertTree, "[1,3,2]");
