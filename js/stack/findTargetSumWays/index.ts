

const dfs = (nums, index, vistied, target,  total) => {
  if (index === nums.length) {
    if (total === target) return 1;
    else return 0;
  }
    
  const totalMinus = (total - nums[index])
  const newKeyMinus =  index+'_'+  totalMinus

  const totalPlus= (total + nums[index])
  const newKeyPlys =  index+'_'+ totalPlus

  if (vistied[newKeyMinus] !== undefined) return vistied[newKeyMinus]
  if (vistied[newKeyPlys] !== undefined) return vistied[newKeyPlys]


  vistied[newKeyPlys] = dfs(nums, index + 1, vistied, target, totalPlus)
  vistied[newKeyMinus] = dfs(nums, index + 1, vistied, target,  totalMinus)

  
  return vistied[newKeyMinus] + vistied[newKeyPlys]

}

function findTargetSumWays(nums: number[], target: number): number {
  const vistied = {};
  vistied[''] = 0;
  let res = 0;
  const index = 0;
  res = dfs(nums, index, vistied, target, 0)
  // console.log(vistied);
  

  return res;
};

console.log(findTargetSumWays([1,1,1,1,1], 3) + '= 5');
console.log(findTargetSumWays([1], 1) + '= 1');
console.log(findTargetSumWays([1, 0], 1) + '= 2');
console.log(findTargetSumWays([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1], 1) + '= 2');
console.log(findTargetSumWays([44,20,38,6,2,47,18,50,41,38,32,24,38,38,30,5,26,15,37,35] ,44) + '= 2');

