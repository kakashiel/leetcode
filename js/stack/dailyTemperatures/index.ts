
function dailyTemperatures(temperatures: number[]): number[] {
  const res = [...Array(temperatures.length)].map(() => 0);
  const stack = [];
  for (let i = 0; i < temperatures.length; i++) {
    let previous = stack[stack.length - 1]
    const current = temperatures[i]
    while( previous?.val < current) { 
      // console.log(`j= ${j}`);
      // console.log(`index= ${i-j}`);
      stack.pop()
      res[previous.index] = i - previous.index;
      previous = stack[stack.length - 1]
    }
    stack.push({val: current, index: i})
    // console.log(stack);
    
  }
  return res;

};

// console.log(dailyTemperatures([30,40,50,60]));
// console.log([1,1,1,0]);
// console.log(dailyTemperatures([40,30,50,60]));
// console.log([2,1,1,0]);
// console.log(dailyTemperatures([4, 1, 3]));
// console.log([0,1,0]);
// console.log(dailyTemperatures([2, 1, 4, 2, 1]));
// console.log([2,1,0, 0, 0]);
// console.log(dailyTemperatures([2, 1, 4, 2, 5]));
// console.log([2,1,2, 1, 0]);

console.log(dailyTemperatures([11, 9,7,6,8,5, 10]));
console.log([0, 5, 2, 1, 2, 1, 0]);

