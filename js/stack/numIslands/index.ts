

const dfs = (grid, x, y, visited) => {
  if(x === -1 || y === -1 || x === grid[0].length || y === grid.length)
  return
  if (grid[y][x] === "0")
  return;
  if (visited[x + y * grid[0].length])
  return;
  const directions = [[ 0, 1 ], [ 0, -1 ], [ 1, 0 ], [ -1, 0 ]]
  visited[x + y * grid[0].length] = true
  directions.forEach((direction) => {
    dfs(grid, x + direction[1], y + direction[0], visited)
  })
}

function numIslands(grid: string[][]): number {
  let numOfIslands = 0;
  let visited = {}
  for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[0].length; x++) {
      if (visited[x + y * grid[0].length] === undefined && grid[y][x] === "1"){
      dfs(grid, x, y, visited)
        numOfIslands++;

      }
    }
  }
  // console.log(grid);

  return numOfIslands;
};

console.log(numIslands([
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]));
console.log(1);

console.log(numIslands([
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]));
console.log(3);
