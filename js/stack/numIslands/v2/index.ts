

const dfs = (grid: string[][], x, y, vistied) => {
  if (x < 0 || y < 0 || x === grid[0].length || grid.length === y)
  return;
  
  if (grid[y][x] === '0') return
  grid[y][x] = "0"

  if (vistied[x + y * grid[0].length]) return
  vistied[x + y * grid[0].length] = true;
  
  
  const direction = [[0, 1], [0, -1], [1, 0], [-1, 0]]
  for (let i = 0; i < direction.length; i++) {
    dfs(grid, x + direction[i][0], direction[i][1] + y, vistied)
  }

}

function numIslands(grid: string[][]): number {
  let res = 0;
  let vistied = {}
  for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[0].length; x++) {
      if (grid[y][x] === "1" ) {
        dfs(grid, x, y, vistied)
        res++

      }

    }
  }
  return res;

};

console.log(numIslands([
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]));
console.log(1);

console.log(numIslands([
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]));
console.log(3);
