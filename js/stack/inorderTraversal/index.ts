import { tests, TreeNode } from "../../utils/tree";

function inorderTraversal(root: TreeNode | null): number[] {
  const res = [];

  if (root === null) return [];
  const stack = [];
  let current = root;

  while (stack.length > 0 || current !== null) {
    while (current !== null) {
      stack.push(current);
      current = current.left;
    }

    current = stack.pop()
    
    res.push(current.val);
    // console.log(res);
    
    current = current.right
  }

  return res;
}

tests.test1(inorderTraversal, '[1,3,2]');
tests.test2(inorderTraversal, '[1,3,2]');
tests.test3(inorderTraversal, '[1,3,2]');
// tests.test2(inorderTraversal)
