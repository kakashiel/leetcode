
class MyNode {
    val: number
    neighbors: MyNode[]
    constructor(val?: number, neighbors?: MyNode[]) {
        this.val = (val===undefined ? 0 : val)
        this.neighbors = (neighbors===undefined ? [] : neighbors)
    }
}


const dfs = (node: MyNode, vistied): MyNode => {
  if (vistied[node.val]) return vistied[node.val]
  const clone = new MyNode(node.val, [])
  vistied[node.val] = clone
  for (let i = 0; i < node.neighbors.length; i++) {
    const neighbor = dfs(node.neighbors[i], vistied)
    clone.neighbors.push(neighbor);
    
  }
  return clone
}

function cloneGraph(MyNode: MyNode | null): MyNode | null {
  if (MyNode === null) return MyNode;

  const vistied = {}

  return dfs(MyNode, vistied)
	
};

