

function isValid(s: string): boolean {
  function checkParenthese(open: string, close: string) {
    return (open === '(' && close === ')') ||
    (open === '{' && close === '}') ||
    (open === '[' && close === ']') 
  }
  const stack = [];
  for (let i = 0; i < s.length; i++) {
    if (s[i] === "(" || s[i] === "{" || s[i] === "[")
    stack.push(s[i])
    else if (s[i] === "]" || s[i] === "}" || s[i] === ")"){
      const open = stack.pop()
      if (!checkParenthese(open, s[i])) return false;
    }
  }
  return stack.length === 0
};

console.log(isValid("")+ '= true');
console.log(isValid("()")+ '= true');
console.log(isValid("[]")+ '= true');
console.log(isValid("[()]")+ '= true');
console.log(isValid("[}]")+ '= false');
console.log(isValid("[")+ '= false');

