
class MinStack {
  private array = []
  private min = undefined;

  constructor() {

  }

  push(val: number): void {
    this.array.push(val)
    if (!this.min || this.min > val) this.min = val
  }

  pop(): void {
    const val =  this.array.pop()
    if (this.array.length === 0){
      this.min = undefined
      return val
    } 
    this.min = this.array.reduce((prev, curr) => Math.min(prev, curr))
    return val

  }

  top(): number {
    return this.array[this.array.length - 1]

  }

  getMin(): number {
    return this.min;

  }
}

var obj = new MinStack()
obj.push(1)
obj.push(2)
obj.pop()
console.log(obj.getMin());
console.log(obj.top());
obj.push(1)
obj.pop()
console.log(obj.getMin());
obj.push(-1)
console.log(obj.getMin());


