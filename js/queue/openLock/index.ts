class MyQueue {
  private array;
  private size: number;
  private tail = 0;
  private head = -1;
  public length = 0;
  constructor(pivot: number) {
    this.size = pivot || 3;
    this.array = Array.apply(null, Array(pivot)).map(() => undefined);
  }
  public enqueue(num): boolean {
    if (this.size === this.length) return false;
    this.head = (1 + this.head) % this.size;
    this.array[this.head] = num;
    this.length++;
    return true;
  }

  public dequeu() {
    if (this.isEmpty()) return null;
    const value = this.array[this.tail];
    // this.array[this.tail] = undefined;
    this.tail = (1 + this.tail) % this.size;
    this.length--;

    return value;
  }

  public isEmpty() {
    return this.length === 0;
  }
}

function replaceAt(s: string, sub:string, index: number) {

  return s.substring(0, index) + sub + s.substring(index + 1, s.length) 
}

//TODO should have start from '0000' less combinaisons ....
function openLock(deadends: string[], target: string): number {
  if (target === '0000') return 0;
  const queue = new MyQueue(10000);
  const numTarget = Number(target);
  // console.log(numTarget);
  
  queue.enqueue(target);
  const lookup = {[`${target}`]: 0}
  while(!queue.isEmpty()){
    const currentState = queue.dequeu();
    const currentStateStep = lookup[currentState];
    if (lookup['0000'] && lookup['0000']< currentStateStep)
    continue;
    for (let i = 0; i < target.length; i++) {
      [1, -1].forEach((e) => {
        
        const nextState = replaceAt(currentState, `${((Number(currentState[i]) +10 + e)) % 10}`, i)
        if (deadends.some(e => e === nextState))
        return;
        
        const nextStateStep = Math.min(currentStateStep + 1, lookup[nextState] || 99999)
        if(lookup[nextState] === undefined)
          queue.enqueue(nextState);
        lookup[nextState] = nextStateStep;
      })
    }
  }

  // console.log(lookup);

  return lookup['0000'] || -1;
};

console.log(openLock(["0201","0101","0102","1212","2002"], "0202"));
console.log(6);
console.log(openLock(["8888"], "0009"));
console.log(1);
console.log(openLock(["8887","8889","8878","8898","8788","8988","7888","9888"], "8888"));
console.log(-1);


