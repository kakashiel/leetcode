
class MyQueu {
  private size = 2000;
  private array = Array(this.size).map(() => undefined);
  public length = 0;
  public head = -1;
  public tail = 0;
  public constructor() {}

  public enqueu(num) {
    if (this.length === this.size)
    return;
    this.length++
    this.head = (this.head + 1) % this.size
    this.array[this.head] = num;
  }

  public dequeu() {
    if (this.isEmpty()) return undefined;
    const val = this.array[this.tail]
    this.tail = (this.tail + 1) % this.size
    this.length--
    return val
  }

  public isEmpty() {
    return this.length === 0;
  }
}


function breadthFirstSearch(grid, x, y) {
  const direction = {
    down: [1, 0],
    right: [0, 1],
    left: [0, -1],
    up: [-1, 0],
  }
  const queue = new MyQueu();
  queue.enqueu({x:x, y:y})
  grid[y][x] = 2;
  while(!queue.isEmpty()) {
    const {x, y} = queue.dequeu();
    for (const [key, value] of Object.entries(direction)) {
      const adjacentX = x + value[1]
      const adjacentY = y + value[0]
      if (adjacentX === -1 || adjacentX === grid[0].length || adjacentY === -1 || adjacentY === grid.length)
      continue;
      if(grid[adjacentY][adjacentX] === "1"){
        queue.enqueu({x:adjacentX, y:adjacentY})
        grid[adjacentY][adjacentX] = "2"
      }

    }

  }

}

function numIslands(grid: string[][]): number {
  let numOfIslands = 0;
  for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[0].length; x++) {
      if(grid[y][x] === "1"){
        breadthFirstSearch(grid, x, y)
        numOfIslands++;

      }
    }
  }
  // console.log(grid);

  return numOfIslands;
};

console.log(numIslands([
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]));
console.log(1);

console.log(numIslands([
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]));
console.log(3);

