class MovingAverage {
  private queue = [];
  private window = 0;
  constructor(size: number) {
    this.window = size;
  }

  next(val: number): number {
    if (this.queue.length === this.window) this.queue.shift();
    this.queue.push(val);
    return this.queue.reduce((prev, curr) => prev + curr) / this.queue.length
  }
}
const movingAverage = new MovingAverage(3);
console.log(movingAverage.next(1)); // return 1.0 = 1 / 1
console.log(movingAverage.next(10)); // return 5.5 = (1 + 10) / 2
console.log(movingAverage.next(3)); // return 4.66667 = (1 + 10 + 3) / 3
console.log(movingAverage.next(5)); // return 6.0 = (10 + 3 + 5) / 3
