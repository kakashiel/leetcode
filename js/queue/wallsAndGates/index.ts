
class MyQueue {
  private array;
  private size: number;
  private tail = 0;
  private head = -1;
  public length = 0;
  constructor(pivot: number) {
    this.size = pivot || 3;
    this.array = Array.apply(null, Array(pivot)).map(() => undefined);
  }
  public enqueue(num): boolean {
    if (this.size === this.length) return false;
    this.head = (1 + this.head) % this.size;
    this.array[this.head] = num;
    this.length++;
    return true;
  }

  public dequeu() {
    if (this.isEmpty()) return null;
    const value = this.array[this.tail];
    // this.array[this.tail] = undefined;
    this.tail = (1 + this.tail) % this.size;
    this.length--;

    return value;
  }

  public isEmpty() {
    return this.length === 0;
  }
}

const WALL = -1;
const GATE = 0;
const ROOM = 2147483647;
const direction = {
  right: [0, 1],
  left: [0, -1],
  up: [-1, 0],
  down: [1, 0],
};

const breadthFirstSearch = (rooms: number[][], y: number, x: number) => {
  const queue = new MyQueue(900);
  const LENGTHY = rooms.length;
  const LENGTHX = rooms[0].length;
  const lookupStep = {[x+(y*LENGTHY * LENGTHX)]: 0}
  queue.enqueue({x: x, y: y})
  while(!queue.isEmpty()) {
    const {x, y} = queue.dequeu();
    for (const [key, value] of Object.entries(direction)) {
      
      const adjacentY = y + value[0]
      const adjacentX = x + value[1]
      
      if (adjacentY === -1 || adjacentX === -1 || adjacentX === rooms[0].length || adjacentY === rooms.length)
      continue;
      if (rooms[adjacentY][adjacentX] === WALL)
      continue;
      // console.log('LENGTHY', LENGTHY);
      // console.log('adjacentY', adjacentY);
      // console.log('adjacentX', adjacentX);
      // console.log('direcatin ====>',key, (adjacentY * LENGTHY * LENGTHX) + adjacentX);
      if (lookupStep[ (adjacentY * LENGTHY * LENGTHX) + adjacentX] !== undefined)
      continue;
      if (rooms[adjacentY][adjacentX] === GATE)
      continue;
      // console.log('direcatin',key, value);
      
      let step = lookupStep[x+(y* LENGTHY * LENGTHX)] + 1;
      queue.enqueue({x: adjacentX, y: adjacentY})
      lookupStep[adjacentX + (adjacentY * LENGTHY * LENGTHX)] = step;
      // console.log('Queue: ', queue.length);
      // console.log('lookupStep: ', lookupStep);
      rooms[adjacentY][adjacentX] = Math.min(rooms[adjacentY][adjacentX], step)
      
    }
  }
  // console.log('Possible awanser res: ', res);
  // return res.length ? Math.min(...res) : ROOM
  // return res.length ? Math.min(...res) : 45
}

function wallsAndGates(rooms: number[][]): void {
        // rooms[0][15] = breadthFirstSearch(rooms, 0, 14);
  for (let y = 0; y < rooms.length; y++) {
    for (let x = 0; x < rooms[y].length; x++) {
      if (rooms[y][x] === GATE){
        // console.log('Looking for room n: ', x+y);
        breadthFirstSearch(rooms, y, x);
      }
    }
  }
};

const test1 = [
  [2147483647,-1,0,2147483647],
  [2147483647,2147483647,2147483647,-1],
  [2147483647,-1,2147483647,-1],
  [0,-1,2147483647,2147483647]
]
wallsAndGates(test1)
let output = [[3,-1,0,1],[2,2,1,-1],[1,-1,2,-1],[0,-1,3,4]]
console.log(test1);
console.log(output);

// const test2 = [[-1]]
// wallsAndGates(test2)
// output = [[-1]]
// console.log(test2);
// console.log(output);

// const test3 = [[2147483647,0,2147483647,2147483647,0,2147483647,-1,2147483647]]
// wallsAndGates(test3)
// console.log(test3);

// const test4 = [
//   [0,2147483647,-1,2147483647,2147483647,-1,-1,0,0,-1,2147483647,2147483647,0,-1,2147483647,2147483647,2147483647,2147483647,0,2147483647,0,-1,-1,-1,-1,2147483647,-1,-1,2147483647,2147483647,-1,-1,0,0,-1,0,0,0,2147483647,0,2147483647,-1,-1,0,-1,0,0,0,2147483647],
//   [2147483647,0,-1,2147483647,0,-1,-1,-1,-1,0,0,2147483647,2147483647,-1,-1,2147483647,-1,-1,2147483647,2147483647,-1,0,-1,2147483647,0,2147483647,-1,2147483647,0,2147483647,0,2147483647,-1,2147483647,0,2147483647,-1,2147483647,0,2147483647,2147483647,0,-1,2147483647,-1,-1,-1,0,2147483647]
// ]
//   wallsAndGates(test4)
// console.log(test4);
//   let outpup
// output = [
//   [0,1,-1,2,1,-1,-1,0,0,-1,1,1,0,-1,2147483647,2147483647,2,1,0,1,0,-1,-1,-1,-1,2,-1,-1,1,2,-1,-1,0,0,-1,0,0,0,1,0,1,-1,-1,0,-1,0,0,0,1],
//   [1,0,-1,1,0,-1,-1,-1,-1,0,0,1,1,-1,-1,2147483647,-1,-1,1,2,-1,0,-1,1,0,1,-1,1,0,1,0,1,-1,1,0,1,-1,1,0,1,1,0,-1,1,-1,-1,-1,0,1]
// ]
// outpup = [
//   [0,1,-1,2,1,-1,-1,0,0,-1,1,1,0,-1,4,3,2,1,0,1,0,-1,-1,-1,-1,2,-1,-1,1,2,-1,-1,0,0,-1,0,0,0,1,0,1,-1,-1,0,-1,0,0,0,1],
//   [1,0,-1,1,0,-1,-1,-1,-1,0,0,1,1,-1,-1,4,-1,-1,1,2,-1,0,-1,1,0,1,-1,1,0,1,0,1,-1,1,0,1,-1,1,0,1,1,0,-1,1,-1,-1,-1,0,1]
// ]
