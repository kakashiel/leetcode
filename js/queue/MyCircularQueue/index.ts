class MyCircularQueue {
  private _queue: Array<number>;
  private _tail: number;
  private _head: number;
  private readonly _MAX_SPACE: number;
  constructor(k: number) {
    this._queue = new Array(k);
    this._MAX_SPACE = k;
  }

  enQueue(value: number): boolean {
    if (this.isFull()) {
      return false;
    }
    if (this.isEmpty()) {
      this._head = 0;
      this._tail = 0;
    } else {
      this._tail = (this._tail + 1) % this._MAX_SPACE;
    }
    this._queue[this._tail] = value;
    // console.log(this._queue);

    return true;
  }

  deQueue(): boolean {
    if (this.isEmpty()) {
      return false;
    }
    this._queue[this._head] = undefined;
    if (this._head === this._tail) {
      this._head = undefined;
      this._tail = undefined;
    } else this._head = (this._head + 1) % this._MAX_SPACE;
    return true;
  }

  Front(): number {
    if (this._queue[this._head] === undefined) return -1;
    return this._queue[this._head];
  }

  Rear(): number {
    if (this._queue[this._tail] === undefined) return -1;
    return this._queue[this._tail];
  }

  isEmpty(): boolean {
    return this._tail === undefined && this._head === undefined;
  }

  isFull(): boolean {
    // console.log(this._tail);
    // console.log(this._head);
    // console.log((this._tail + 1) % this._MAX_SPACE)
    
    if (this._tail !== undefined && this._head !== undefined) {
      return (this._tail + 1) % this._MAX_SPACE === this._head;
    }
    return false;
  }
}

const myCircularQueue = new MyCircularQueue(3);
console.log(myCircularQueue.enQueue(1)); // return True
console.log(myCircularQueue.enQueue(2)); // return True
console.log(myCircularQueue.enQueue(3)); // return True
console.log(myCircularQueue.enQueue(4)); // return False
console.log(myCircularQueue.Rear()); // return 3
console.log(myCircularQueue.isFull()); // return True
console.log(myCircularQueue.deQueue()); // return True
console.log(myCircularQueue.enQueue(4)); // return True
console.log(myCircularQueue.Rear()); // return 4
