
// Definition for singly-linked list.
class ListNode {
    val: number
    next: ListNode | null
    constructor(val?: number, next?: ListNode | null) {
        this.val = (val===undefined ? 0 : val)
        this.next = (next===undefined ? null : next)
    }
}


function reverseList(head: ListNode | null): ListNode | null {
  let current = head;
  if (current.next === null)
    return current;
  else
    head = reverseList(head.next)

};

function print(head: ListNode) {
  const node = head;
  if (node.next === null) return `${node.val}`;
  return `${node.val},` + print(node.next);
}

const test = () => {
  
  const node4 = new ListNode(4, null);
  const node3 = new ListNode(3, node4);
  const node2 = new ListNode(2, node3);
  const node1 = new ListNode(1, node2);
  const hear = reverseList(node1)
  console.log(print(hear));
};

test()
