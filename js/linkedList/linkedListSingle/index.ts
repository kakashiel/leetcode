class MyLinkedList {
  private array: number[];

  constructor() {
    this.array = [];
  }

  get(index: number): number {
    console.log(`index: ${index} | ${this.array[index]}`);
    const res = this.array[index];
    if (res !== undefined) return res;
    return -1;
  }

  addAtHead(val: number): void {
    this.array = [val, ...this.array];
    console.log(this.array);
  }

  addAtTail(val: number): void {
    this.array.push(val);
    console.log(this.array);
  }

  addAtIndex(index: number, val: number): void {
    if (index <= this.array.length)
      this.array = [
        ...this.array.slice(0, index),
        val,
        ...this.array.slice(index, this.array.length),
      ];
    console.log(this.array);
  }

  deleteAtIndex(index: number): void {
    this.array.splice(index, 1);
    console.log(this.array);
  }
}

function assert<T = unknown>(val: T, res: T) {
  if (val !== res) throw new Error(`${val} !== ${res}`);
}

var obj = new MyLinkedList();
obj.addAtHead(1);
obj.addAtTail(3);
obj.addAtIndex(1, 2); // linked list becomes 1->2->3
assert(obj.get(1), 2); // return 2
obj.deleteAtIndex(1); // now the linked list is 1->3
assert(obj.get(1), 3); // return 3
obj.addAtIndex(1, 2); // linked list becomes 1->2->3
obj.deleteAtIndex(1); // now the linked list is 1->3
obj.deleteAtIndex(1); // now the linked list is 1->3
assert(obj.get(1), -1); // return 3
