var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var MyLinkedList = /** @class */ (function () {
    function MyLinkedList() {
        this.array = [];
    }
    MyLinkedList.prototype.get = function (index) {
        console.log(this.array);
        return this.array[index] || -1;
    };
    MyLinkedList.prototype.addAtHead = function (val) {
        this.array = __spreadArray([val], this.array, true);
    };
    MyLinkedList.prototype.addAtTail = function (val) {
        this.array.push(val);
    };
    MyLinkedList.prototype.addAtIndex = function (index, val) {
        this.array = __spreadArray(__spreadArray(__spreadArray([], this.array.slice(0, index), true), [val], false), this.array.slice(index, this.array.length), true);
    };
    MyLinkedList.prototype.deleteAtIndex = function (index) {
        this.array.splice(index, 1);
    };
    return MyLinkedList;
}());
function assert(val, res) {
    if (val !== res)
        throw new Error("".concat(val, " !== ").concat(res));
}
var myLinkedList = new MyLinkedList();
myLinkedList.addAtHead(1);
myLinkedList.addAtTail(3);
myLinkedList.addAtIndex(1, 2); // linked list becomes 1->2->3
assert(myLinkedList.get(1), 2); // return 2
myLinkedList.deleteAtIndex(1); // now the linked list is 1->3
assert(myLinkedList.get(1), 3); // return 3
myLinkedList.addAtIndex(1, 2); // linked list becomes 1->2->3
myLinkedList.deleteAtIndex(1); // now the linked list is 1->3
myLinkedList.deleteAtIndex(1); // now the linked list is 1->3
assert(myLinkedList.get(1), -1); // return 3
