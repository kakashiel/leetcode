class ListNode {
  val: number;
  next: ListNode | null;
  constructor(val?: number, next?: ListNode | null) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
  }
}

function secondHalfOflist(head: ListNode): ListNode {
  let fast = head;
  let slow = head;
  while (fast) {
    if (fast.next === null) {
      fast = null;
    } else {
      fast = fast.next.next;
    }

    slow = slow.next;
  }
  return slow;
}

function reverse(head: ListNode): ListNode {
  let previous = null;
  let current = head;
  while (current) {
    const next = current.next;
    current.next = previous;
    previous = current;
    current = next;
  }
  return previous;
}

function isPalindrome(head: ListNode | null): boolean {
  if (!head) return false;
  const secondHalfHead = secondHalfOflist(head);
  // console.log('reverse =' + stringify(secondHalfHead));
  const reverseSecondHalfHead = reverse(secondHalfHead);
  let node = reverseSecondHalfHead;
  let node2 = head;
  // console.log('node =' + stringify(node));
  // console.log('firstNode =' + stringify(node2));
  
  while (node) {
    if (node.val !== node2.val) return false;
    node = node.next;
    node2 = node2.next;
  }

  return true;
}

const stringify = (head: ListNode | null): string => {
  if (!head) return "";
  return `${head.val}, ` + stringify(head.next);
};

const testReverse1 = () => {
  const node1 = new ListNode(1, null);
  const head = reverse(node1);
  console.log(stringify(head) + " = 1");
};
testReverse1();

const testReverse2 = () => {
  const node1 = new ListNode(1, null);
  const node2 = new ListNode(2, node1);
  const node3 = new ListNode(3, node2);
  const head = reverse(node3);
  console.log(stringify(head) + " = 1,2,3");
};
testReverse2();

const test = () => {
  const res = isPalindrome(null);
  console.log(res + " = false");
};
test();

const test2 = () => {
  const node1 = new ListNode(1, null);
  const res = isPalindrome(node1);
  console.log(res + " = true");
};
test2();

const test3 = () => {
  const node1 = new ListNode(1, null);
  const node2 = new ListNode(2, node1);
  const node3 = new ListNode(1, node2);
  const secondHalf = secondHalfOflist(node3);
  console.log(stringify(secondHalf) + " = 1");
  const res = isPalindrome(node3);
  console.log(res + " = true");
};
test3();

const test4 = () => {
  const node1 = new ListNode(1, null);
  const node2 = new ListNode(1, node1);
  const res = isPalindrome(node2);
  console.log(res + " = true");
};
test4();

const test5 = () => {
  const node1 = new ListNode(1, null);
  const node2 = new ListNode(2, node1);
  const secondHalf = secondHalfOflist(node2);
  console.log(secondHalf.val + " = 2");
  const res = isPalindrome(node2);
  console.log(res + " = false");
};
test5();

const test6 = () => {
  const node1 = new ListNode(1, null);
  const node2 = new ListNode(2, node1);
  const node3 = new ListNode(3, node2);
  const secondHalf = secondHalfOflist(node3);
  console.log(stringify(secondHalf) + " = 1");
  const res = isPalindrome(node3);
  console.log(res + " = false");
};
test6();
