class ListNode {
  val: number;
  next: ListNode | null;
  constructor(val?: number, next?: ListNode | null) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
  }
}

const stringify = (list: ListNode) => {
  if (!list) return "";
  return `${list.val},` + stringify(list.next);
};

function mergeTwoLists(list1: ListNode | null, list2: ListNode | null): ListNode | null {
  let current1 = list1;
  let current2 = list2;
  let previous = new ListNode(0, null);
  let newHead = previous;
  let node = null;
  while (current1 && current2) {
    if (current1.val <= current2.val) {
      node = current1;
      current1 = current1.next;
    } else {
      node = current2;
      current2 = current2.next;
    }
    previous.next = node;
    previous = node;
  }
  if (current1) node = current1;
  if (current2) node = current2;
  previous.next = node;
  return newHead.next;
}

const test2 = () => {
  const node1 = new ListNode(5, null);
  const node2 = new ListNode(2, node1);
  const node__1 = new ListNode(6, null);
  const node__2 = new ListNode(2, node__1);
  const node__3 = new ListNode(1, node__2);
  const merge = mergeTwoLists(node2, node__3);
  console.log(stringify(merge) + " = 1,2,2,5,6");
};
test2();

const test3 = () => {
  const node1 = new ListNode(4, null);
  const node2 = new ListNode(2, node1);
  const node3 = new ListNode(1, node2);
  const node__1 = new ListNode(4, null);
  const node__2 = new ListNode(3, node__1);
  const node__3 = new ListNode(1, node__2);
  const merge = mergeTwoLists(node3, node__3);
  console.log(stringify(merge) + " = 1,1,2,3,4,4");
};
test3();
//
// const test = () => {
//   const list1 = null;
//   const list2 = null;
//   const merge = mergeTwoLists(list1, list2);
//   console.log(merge + "");
// };
// test();
//
// const test1 = () => {
//   const list1 = new ListNode(1, null);
//   const list2 = null;
//   const merge = mergeTwoLists(list1, list2);
//   console.log(stringify(merge) + " = 1");
// };
// test1();
