class ListNode {
  val: number;
  next: ListNode | null;
  constructor(val?: number, next?: ListNode | null) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
  }
}
function removeNthFromEnd(head: ListNode | null, n: number): ListNode | null {
  let toDelete = head;
  let nPos = head;
  for (let i = 0; i < n; i++) {
    nPos = nPos.next;
  }
  if (nPos === null) return head.next;
  while (nPos.next !== null) {
    nPos = nPos.next;
    toDelete = toDelete.next;
  }
  toDelete.next = toDelete.next.next;
  return head;
}

function print(head: ListNode) {
  const node = head;
  if (node.next === null) return `${node.val}`;
  return `${node.val},` + print(node.next);
}

const test = () => {
  const node4 = new ListNode(4, null);
  const node3 = new ListNode(3, node4);
  const node2 = new ListNode(2, node3);
  const node1 = new ListNode(1, node2);
  console.log(print(removeNthFromEnd(node1, 1)));
};
const test2 = () => {
  const node4 = new ListNode(4, null);
  const node3 = new ListNode(3, node4);
  const node2 = new ListNode(2, node3);
  const node1 = new ListNode(1, node2);
  console.log(print(removeNthFromEnd(node1, 2)));
};
const test3 = () => {
  const node4 = new ListNode(4, null);
  const node3 = new ListNode(3, node4);
  const node2 = new ListNode(2, node3);
  const node1 = new ListNode(1, node2);
  console.log(print(removeNthFromEnd(node1, 3)));
};
const test4 = () => {
  const node4 = new ListNode(4, null);
  const node3 = new ListNode(3, node4);
  const node2 = new ListNode(2, node3);
  const node1 = new ListNode(1, node2);
  console.log(print(removeNthFromEnd(node1, 4)));
};
test();
test2();
test3();
test4();
// console.log(print(removeNthFromEnd(node1, 3)));
