// * Definition for singly-linked list.
class ListNode {
  val: number;
  next: ListNode | null;
  constructor(val?: number, next?: ListNode | null) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
  }
}

function removeElements(head: ListNode | null, val: number): ListNode | null {
  let current = head;
  let previous = null;
  while (current) {
    if (current.val === val) {
      if (previous) {
        previous.next = current.next;
      } else {
        head = current.next;
      }
    } else {
      previous = current;
    }
    current = current.next;
  }
  console.log(head);

  return head;
}

function printList(head: ListNode) {
  if (head === null) return "";
  else return `${head.val}, ` + printList(head.next);
}

const test = () => {
  const node1 = new ListNode(1, null);

  const resNode1 = removeElements(node1, 2);
  let res = printList(resNode1);
  console.log(`${res} = ,1`);

  res = printList(removeElements(node1, 1));
  console.log(`${res} = ""`);
};

const test2 = () => {
  const node1 = new ListNode(1, null);
  const node2 = new ListNode(2, node1);
  let res = printList(removeElements(node2, 1));
  console.log(`${res} = ",2"`);
};

const test3 = () => {
  const node1 = new ListNode(1, null);
  const node2 = new ListNode(2, node1);
  const node3 = new ListNode(3, node2);
  let res = printList(removeElements(node3, 2));
  console.log(`${res} = "3,1,"`);
};

test();
test2();
test3();
