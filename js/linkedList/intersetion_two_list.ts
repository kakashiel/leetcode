// Definition for singly-linked list.
class ListNode {
  val: number;
  next: ListNode | null;
  constructor(val?: number, next?: ListNode | null) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
  }
}

function getIntersectionNode(headA: ListNode | null, headB: ListNode | null): ListNode | null {
  if (headA === null || headB === null) return null;
  let ptr1 = headA;
  let ptr2 = headB;
  while (ptr1 !== ptr2) {
    ptr1 = ptr1.next;
    ptr2 = ptr2.next;
    console.log('============');
    
    console.log(ptr1)
    console.log(ptr2)

    if (ptr1 === ptr2) return ptr1
    if (ptr1 === null) ptr1 = headB;
    if (ptr2 === null) ptr2 = headA;
  }
  return ptr1;
}

const node5 = new ListNode(4, null);

const node6 = new ListNode(2, node5);
const node4 = new ListNode(4, node6);

const node7 = new ListNode(2, node4);
const node8 = new ListNode(1, node7);
const headB = new ListNode(3, node8);

const node3 = new ListNode(1, node4);
const headA = new ListNode(1, node3);
console.log(getIntersectionNode(headA, headB));

