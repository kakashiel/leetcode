// Definition for singly-linked list.

class ListNode {
  val: number;
  next: ListNode | null;
  constructor(val?: number, next?: ListNode | null) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
  }
}

/**
 Do not return anything, modify head in-place instead.
 */
function reorderList(head: ListNode | null): void {
  const array = [];
  let node = head;
  while (node !== null) {
    array.push(node);
    node = node.next;
  }
  for (let i = 0; i < array.length; i++) {
    if (i % 2 === 0) array[i].next = array[array.length - i - 1];
    else {
      array[array.length - i ].next = array[i];
    }
  }
}

function readListNode(head: ListNode): string {
  if (head.next === null) return `${head.val}`;
  else return `${head.val},` + readListNode(head.next);
}

function assert<T>(test: T, solution: T) {
  if (test !== solution) throw new Error(`test != solution; ${test} != ${solution}`);
}

const node4 = new ListNode(4, null);
const node3 = new ListNode(3, node4);
const node2 = new ListNode(2, node3);
const node1 = new ListNode(1, node2);
const sol = readListNode(node1);
reorderList(node1);
console.log(node1);

const res = readListNode(node1);

assert(res, sol);
