class TreeNode {
  val: number;
  left: TreeNode | null;
  right: TreeNode | null;
  constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
    this.val = val === undefined ? 0 : val;
    this.left = left === undefined ? null : left;
    this.right = right === undefined ? null : right;
  }
}

function maxDepth(root: TreeNode | null): number {
    if (root === null) return 0;
    const left = maxDepth(root.left) + 1;
    const right = maxDepth(root.right) + 1;
    return left > right ? left : right;
}

const node1 = new TreeNode(1);
const node2 = new TreeNode(1);
const node3 = new TreeNode(1, node2, node1);
const node4 = new TreeNode(1, null, null);
const head = new TreeNode(1, node4, node3);
console.log(maxDepth(head) + '= 3');
console.log(maxDepth(node1)+'= 1');
