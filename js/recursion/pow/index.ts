
class Tco {
  private f;
  constructor(f: any){
    this.f = f;
  }
  public execute = () => {
    let value = this;
    while (value instanceof Tco){
      value = value.f();
    }
    return value;
  }
}

function myPow(x: number, n: number): number {
  const array = [];
  const pow = (x: number, n: number, arrayRes: number[]) =>new Tco(() => {
    if (n === 0) return arrayRes.push(1);
    if (n === 1) return arrayRes.push(x);
    if (n === -1) return arrayRes.push(1 / x);
    if (n < 0) {
      arrayRes.push(1 / x);
      return pow(x, n + 1, arrayRes);
    }
    if (n > 0) {
      arrayRes.push(x);
      return pow(x, n - 1, arrayRes);
    }
  });
  const res = pow(x,n,array).execute()
  console.log(res);
  
  return array.reduce((p,n) => p * n)
}
console.log(myPow(2, 10) + "= 1024.00000");
console.log(myPow(2.1, 3) + "= 9.26100");
console.log(myPow(2.0, -2) + "= 0.25000");
console.log(myPow(0.00001, -2147483647) + "= 0.25000");
