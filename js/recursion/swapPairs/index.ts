
class ListNode {
    val: number
    next: ListNode | null
    constructor(val?: number, next?: ListNode | null) {
        this.val = (val===undefined ? 0 : val)
        this.next = (next===undefined ? null : next)
    }
}


function swapPairs(head: ListNode | null): ListNode | null {
  if (!head) return null;
  if (head.next){
    const tmp = head.next;
    head.next = head;
    head = tmp
    swapPairs (head.next.next)
  }
  return head;

};

const stringify = (head: ListNode) => {
  let node = head;
  let string = ''
  while(node){
    string += node.val + ', '
    node = node.next
  }
  return string

}

console.log(stringify(swapPairs(null)) + '= ');
const node1 = new ListNode(1, null)
console.log(stringify(swapPairs(node1)) + '= [1]');

const node2 = new ListNode(4, null)
const node3 = new ListNode(3, node2)
const node4 = new ListNode(2, node3)
const node5 = new ListNode(1, node4)
console.log(stringify(swapPairs(node5)) + '= 2,1,4,3');

