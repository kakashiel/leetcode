function climbStairs(n: number): number {
  const hmap = new Map();
  const myClimb = (n: number, hmap: Map<number, number>) => {
    if (hmap.has(n)) {
      return hmap.get(n);
    }
    if (n === 0) return 0;
    if (n === 1) return 1;
    if (n === 2) return 2;
    const res = myClimb(n - 1, hmap) + myClimb(n - 2, hmap);
    hmap.set(n, res);
    return res;
  };
  return myClimb(n, hmap);
}

// console.log(climbStairs(2) + "= 2");
// console.log(climbStairs(3) + "= 3");
// console.log(climbStairs(4) + "= 5");
console.log(climbStairs(10) + "= 5");

// 1. 1 step + 1 step + 1 step + 1
// 2. 1 step + 2 steps + 1
// 3. 2 steps + 1 step + 1
// 2. 1+ 1 step + 2 steps
// 3. 2 + 2 steps
