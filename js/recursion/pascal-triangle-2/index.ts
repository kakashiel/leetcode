


function getRow(rowIndex: number): number[] {
  if (rowIndex === 0) return [1]
  if (rowIndex === 1) return [1,1]
  const prev = getRow(rowIndex - 1)
  const currentRow = [1]
  for (let  index = 0;  index < prev.length - 1;  index++) {
    currentRow.push(prev[index] + prev[index + 1])
  }
  currentRow.push(1)
  return currentRow

};

console.log('[1,3,3,1]', getRow(3));
console.log('[1]', getRow(0));
console.log('[1,1]', getRow(1));

