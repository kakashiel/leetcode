function fibonnaci(n, memoization) {
  if (n === 1) return 1;
  if (n === 0) return 0;
  if (memoization[n]) {
    return memoization[n]
  }
  const res = fibonnaci(n - 1, memoization) + fibonnaci(n - 2, memoization);
  memoization[n] = res;
  return res;
}

function fib(n: number): number {
  const hashmap = {};
  return fibonnaci(n, hashmap)
}
console.log(fib(2) + "=1");
console.log(fib(3) + "=2");
console.log(fib(4) + "=3");
console.log(fib(5) + "=5");
console.log(fib(6) + "=8");
