class TreeNode {
  val: number;
  left: TreeNode | null;
  right: TreeNode | null;
  constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
    this.val = val === undefined ? 0 : val;
    this.left = left === undefined ? null : left;
    this.right = right === undefined ? null : right;
  }
}

function searchBST(root: TreeNode | null, val: number): TreeNode | null {
  if (!root) return null;
  if (root.val === val) return root;

  const left = searchBST(root.left, val);
  const right = searchBST(root.right, val);
  if (left) return left;
  return right;
}

const stringifyLevel = (root: TreeNode | null, level: number, res: number[]) => {
  if (!root) return null;
  if (level === 0) res.push(root.val);
  stringifyLevel(root.left, level - 1, res);
  stringifyLevel(root.right, level - 1, res);
};

const stringify = (root: TreeNode | null, height: number): number[] => {
  const string = [];
  for (let i = 0; i <= height; i++) {
    stringifyLevel(root, i, string);
  }
  return string;
};

const height = (root: TreeNode | null): number => {
  if (!root) return 0;
  const left = height(root.left);
  const right = height(root.right);
  return left < right ? left + 1 : right + 1;
};

const node1 = new TreeNode(1, null);
const node3 = new TreeNode(3, null);
const node7 = new TreeNode(7, null, null);
const node2 = new TreeNode(2, node1, node3);
const node4 = new TreeNode(4, node2, node7);
const heightNode = height(node4);
console.log(heightNode + "= 2");
console.log(stringify(node4, heightNode));
const search = searchBST(node4, 2)
console.log(stringify(search, height(search)));
