from typing import Optional
import sys
sys.path.append('../../utils')

from utils.link_list import ListNode, MyLinkedList

class Solution:
    def reverseList(self, head: Optional[ListNode]) -> Optional[ListNode]: 
        pass

    def printList(self, head):
        while head != None:
            print(head.val)
            head = head.next

    def buildList(self, array):
        head = ListNode(array[len(array) - 1])
        for value in reversed(range(len(array) - 1)):
            new = ListNode(array[value], head)
            head = new
        return head


    def test(self):
        array = [1,2,3,4,5]
        head = MyLinkedList.buildList(array)
        MyLinkedList.printList(head)


# if __name__ == "__main__":
Solution().test()
