# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class MyLinkedList:
    def reverseList(self, head: Optional[ListNode]) -> Optional[ListNode]: 
        pass

    def printList(self, head):
        while head != None :
            print(head.val)
            head = head.next;

    def buildList(self, array):
        head = ListNode(array[len(array) - 1]) 
        node = head
        for value in reversed(range(len(array) - 1)):
            new = ListNode(array[value], head)
            head = new;
        return head
