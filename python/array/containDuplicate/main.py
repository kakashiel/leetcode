class Solution:
    def containsDuplicate(self, nums: list[int]) -> bool:
        res = False
        hash = {}
        for num in nums:
            if (num in hash):
                res = True
                break;
            hash[num] = 1
        return res


sol = Solution()
assert sol.containsDuplicate([1,2,3]) == False
assert sol.containsDuplicate([1,2,1]) == True
assert sol.containsDuplicate([1,2,3,3]) == True
