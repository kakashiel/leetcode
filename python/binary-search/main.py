from typing import List
class Solution:

    def search2(self, nums: List[int], target: int) -> int:
        try:
            return nums.index(target) 
        except:
            return -1
    def search(self, nums: List[int], target: int) -> int:
        l=0
        r=len(nums) - 1
        while l <= r:
            mid = l + (r - l) // 2
    
            # Check if x is present at mid
            if nums[mid] == target:
                return mid
    
            # If x is greater, ignore left half
            elif nums[mid] < target:
                l = mid + 1
    
            # If x is smaller, ignore right half
            else:
                r = mid - 1
        return -1
    def test(self) -> None:
        # print("{} = {}", search([-1,0,3,5,9,12], 9), 4)
        print("{} = {}".format(self.search([-1,0,3,5,9,12], 9), 4))
        print("{} = {}".format(self.search([-1,0,3,5,9,12], 2), -1))

if __name__ == "__main__":
    Solution().test()
