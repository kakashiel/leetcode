package utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import entity.TreeNode;

public class BinaryTreeBuilderTest {
    @Test
    public void test_buildBinaryTree_validInput() {
        Integer[] array = { 1, 2, 3, 4, 5, 6, 7 };
        TreeNode expected = new TreeNode(1);
        expected.left = new TreeNode(2);
        expected.right = new TreeNode(3);
        expected.left.left = new TreeNode(4);
        expected.left.right = new TreeNode(5);
        expected.right.left = new TreeNode(6);
        expected.right.right = new TreeNode(7);

        TreeNode result = BinaryTreeBuilder.build(array);

        assertEquals(expected.val, result.val);
        assertEquals(expected.left.val, result.left.val);
        assertEquals(expected.right.val, result.right.val);
        assertEquals(expected.left.left.val, result.left.left.val);
        assertEquals(expected.left.right.val, result.left.right.val);
        assertEquals(expected.right.left.val, result.right.left.val);
        assertEquals(expected.right.right.val, result.right.right.val);
    }

    // Should build a binary tree with only one node
    @Test
    public void test_buildBinaryTree_withOneNode() {
        Integer[] array = { 1 };
        TreeNode expected = new TreeNode(1);

        TreeNode result = BinaryTreeBuilder.build(array);

        assertEquals(expected.val, result.val);
        assertNull(result.left);
        assertNull(result.right);
    }

    // Should build a binary tree with multiple levels and children on both sides
    @Test
    public void test_buildBinaryTree_multipleLevelsAndChildren() {
        Integer[] array = { 1, 2, 3, 4, 5, 6, 7 };
        TreeNode expected = new TreeNode(1);
        expected.left = new TreeNode(2);
        expected.right = new TreeNode(3);
        expected.left.left = new TreeNode(4);
        expected.left.right = new TreeNode(5);
        expected.right.left = new TreeNode(6);
        expected.right.right = new TreeNode(7);

        TreeNode result = BinaryTreeBuilder.build(array);

        assertEquals(expected.val, result.val);
        assertEquals(expected.left.val, result.left.val);
        assertEquals(expected.right.val, result.right.val);
        assertEquals(expected.left.left.val, result.left.left.val);
        assertEquals(expected.left.right.val, result.left.right.val);
        assertEquals(expected.right.left.val, result.right.left.val);
        assertEquals(expected.right.right.val, result.right.right.val);
    }

        // Should build a binary tree with null nodes
        @Test
        public void test_buildBinaryTree_withNullNodes() {
            Integer[] array = {1, null, 3, null, null, 6, 7};
            TreeNode expected = new TreeNode(1);
            expected.right = new TreeNode(3);
            expected.right.left = new TreeNode(6);
            expected.right.right = new TreeNode(7);
    
            TreeNode result = BinaryTreeBuilder.build(array);
    
            assertEquals(expected.val, result.val);
            assertNull(result.left);
            assertEquals(expected.right.val, result.right.val);
            assertEquals(expected.right.left.val, result.right.left.val);
            assertEquals(expected.right.right.val, result.right.right.val);
        }
}
