package utils;

import java.util.ArrayList;
import java.util.Arrays;

import entity.TreeNode;

public class BinaryTreeBuilder {

    public static TreeNode build(Integer[] array) {
        if (array[0] == null) {
            return null;
        }
        TreeNode head = new TreeNode(array[0], null, null);

        ArrayList<TreeNode> queue = new ArrayList<>(Arrays.asList(head));
        int i = 0;
        while (queue.size() > 0) {
            if (array[i] == null) {
                i++;
                continue;
            }
            TreeNode node = queue.remove(0);
            int index = i * 2;
            if (index + 1 < array.length && array[index + 1] != null) {
                node.left = new TreeNode(array[index + 1], null, null);
                queue.add(node.left);
            }
            if (index + 2 < array.length && array[index + 2] != null) {
                node.right = new TreeNode(array[index + 2], null, null);
                queue.add(node.right);
            }
            i++;
        }
        return head;
    }
}
