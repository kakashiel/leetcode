package main.slidingWindow;

import java.util.HashMap;

public class LongestSubstringWithoutRepeatingCharacters {
      public int lengthOfLongestSubstring(String s) {
    int count = 0;
    int max = 0;
    HashMap<Character, Integer> hashMap = new HashMap<>();
    for (int i = 0; i < s.length(); i++) {
      // System.out.println(s.charAt(i));
      if (hashMap.get(s.charAt(i)) != null) {
        max = max > count ? max : count;
        count = 0;
        i = hashMap.get(s.charAt(i));
        hashMap = new HashMap<>();
        continue;
      }
      count++;
      hashMap.put(s.charAt(i), i);
    }
    max = max > count ? max : count;

    return max;
  }
    
}
