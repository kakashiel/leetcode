package main.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

class GroupAnagrams {
    public boolean isAnagram(String a, String b) {
        if (a.length() != b.length())
            return false;
        HashMap<Character, Integer> aHashMap = new HashMap<>();
        HashMap<Character, Integer> bHashMap = new HashMap<>();
        for (int i = 0; i < a.length(); i++) {
            Character aChar = a.charAt(i);
            Character bChar = b.charAt(i);
            if (aHashMap.containsKey(aChar)) {
                aHashMap.replace(aChar, aHashMap.get(aChar) + 1);
            } else {
                aHashMap.put(aChar, 0);
            }
            if (bHashMap.containsKey(bChar)) {
                bHashMap.replace(bChar, bHashMap.get(bChar) + 1);
            } else {
                bHashMap.put(bChar, 0);
            }
        }
        return aHashMap.equals(bHashMap);
    }

    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> res = new ArrayList<>();
        for (int i = 0; i < strs.length; i++) {
            boolean isAdded = false;
            for (int j = 0; j < res.size(); j++) {
                if (this.isAnagram(res.get(j).getFirst(), strs[i])) {
                    List<String> list = res.get(j);
                    list.add(strs[i]);
                    isAdded = true;
                }
            }
            if (!isAdded) {
                res.add(new ArrayList<>(Arrays.asList(strs[i])));
            }
        }
        return res;

    }

}