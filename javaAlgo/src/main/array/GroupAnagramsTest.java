package main.array;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class GroupAnagramsTest {
    @Test
    public void shouldGroupAnagrams() {
        GroupAnagrams groupAnagrams = new GroupAnagrams();
        String[] list = new String[] { "eat", "tea", "tan", "ate", "nat", "bat" };
        List<List<String>> res = groupAnagrams.groupAnagrams(list);
        List<List<String>> expected = Arrays.asList(
                Arrays.asList("bat"),
                Arrays.asList("nat", "tan"),
                Arrays.asList("ate", "eat", "tea"));

        assertEquals(expected, res);
    }
}
