package main.array;

import java.util.HashMap;

public class ContainsDuplicate {
      public boolean containsDuplicate(int[] nums) {

    HashMap<Integer, Integer> duplicate = new HashMap<>();
    for (int i : nums) {
      if (duplicate.get(i) != null) {
        return true;
      }
      duplicate.put(i, i);

    }

    return false;
  }
}
