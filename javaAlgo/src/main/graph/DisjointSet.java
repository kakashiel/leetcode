package main.graph;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class DisjointSet {
    public int findCircleNum(int[][] isConnected) {
        int province = 0;
        Set<Integer> visitied = new HashSet<>();
        Stack<int[]> stack = new Stack<>();
        for (int i = 0; i < isConnected.length; i++) {
            if (!visitied.contains(i)) {
                stack.add(isConnected[i]);
                while (!stack.isEmpty()) {
                    int[] city = stack.pop();
                    for (int j = 0; j < city.length; j++) {
                        if (city[j] == 1 && !visitied.contains(j)) {
                            stack.add(isConnected[j]);
                            visitied.add(j);

                        }
                    }
                }
                province = province + 1;
            }
        }
        return province;
    }
}