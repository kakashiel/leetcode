package main.graph;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DisjointSetTest {
    @Test
    public void test_findCircleNum() {
        DisjointSet sol = new DisjointSet();
        int[][] isConnected = {{1, 1, 0}, {1, 1, 0}, {0, 0, 1}};
        int result = sol.findCircleNum(isConnected);
        assertEquals(2, result);
    }

    @Test
    public void test_findCircleNum2() {
        DisjointSet2 sol = new DisjointSet2();
        int[][] isConnected = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
        int result = sol.findCircleNum(isConnected);
        assertEquals(3, result);
    }
    
}
