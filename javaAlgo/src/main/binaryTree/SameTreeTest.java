package main.binaryTree;

import org.junit.Test;

import entity.TreeNode;
import utils.BinaryTreeBuilder;

import static org.junit.Assert.*;

public class SameTreeTest{
    @Test
    public void test_create_instance() {
        SameTree sol = new SameTree();
        assertNotNull(sol);
    }

    // The method should call the isSameTree method of the Solution class without errors.
    @Test
    public void test_call_isSameTree() {
        SameTree sol = new SameTree();
        TreeNode p = BinaryTreeBuilder.build(new Integer[]{1, 2, 3});
        boolean result = sol.isSameTree(p, p);
        assertTrue(result);
    }
    @Test
    public void should_not_be_same_tree() {
        SameTree sol = new SameTree();
        TreeNode p = BinaryTreeBuilder.build(new Integer[]{1, 2, 3});
        TreeNode q = BinaryTreeBuilder.build(new Integer[]{1, 2, 3, 4});
        boolean result = sol.isSameTree(p, q);
        assertFalse(result);
    }
}