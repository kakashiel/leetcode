package main.binaryTree;

import java.util.LinkedList;
import java.util.Queue;

import entity.TreeNode;

class SameTree {
    public boolean isSameTree(TreeNode p, TreeNode q) {
        
        Queue<TreeNode> queueQ = new LinkedList<>();
        Queue<TreeNode> queueP = new LinkedList<>();
        queueP.add(p);
        queueQ.add(q);
        while (!queueP.isEmpty() || !queueQ.isEmpty()) {
            if (queueP.isEmpty() || queueQ.isEmpty()) {
                return false;
            }
            TreeNode nodeP = queueP.remove();
            TreeNode nodeQ = queueQ.remove();
            if (nodeP == null && nodeQ == null) {
                continue;
            }
            if (nodeP == null || nodeQ == null) {
                return false;
            }
            if (nodeP.val != nodeQ.val) {
                return false;
            }
            queueP.add(nodeP.left);
            queueQ.add(nodeQ.left);
            queueP.add(nodeP.right);
            queueQ.add(nodeQ.right);
           
        }
        
        return queueP.isEmpty() && queueQ.isEmpty();
    }
}